// config
const config = require('../config.js');
const secret = `${global.gConfig.secret}`;
var randomstring = require("randomstring");
//campaign services
const campaignService = require('../services/campaignServices.js');
// influencer services
const influencerService = require('../services/userServices.js');
// campaign influencer services
const campaignInfluencerService = require('../services/campaignInfluencerServices.js');
// helper and glocal functions
const helper = require('../globalFunctions/function.js');
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const campaignInfluencerModel = require('../models/campaign_influencer.js');
// Obj ID
const ObjectId = require('mongodb').ObjectID;

//Influencer modal
const influencer_model = require('../models/userModel');
// Array for status
const statusArr = [{label: "declined",value: 1},{label: "",value: 2},{label: "completed",value: 3},{label: "accepted",value: 4},{label: "engaged",value: 5}];

/* Campaign Platform Listiadng*/
const campaignPlatformListing = (req, res) => {
    let check = helper.checkRequest(["campaign_id"], req.body);
    if (check == true) {
        campaignService.findCampaignData({ '_id': new ObjectId(req.body.campaign_id) }, { 'platforms': 1 }, (err, result) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign Platform Listing Found!', result);
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/* Listing  Search Api */

const microSearch = (req, res) => {
    let check = helper.checkRequest(["campaign_id", "keyword"], req.body);
    if (check == true) {
        // const platform = req.body.platform;
        let category = '';
        let pages = 0;
        let reg = { '$regex': req.body.keyword };
        let social_name = { 'twitter.Elv_social': reg };
        let instagram_social_name = { 'instagram.Elv_social': reg };
        let facebook_social_name = { 'facebook.Elv_social': reg };

        let query = {};
        let category_filter = {};
        let social = {};
        var influencerData2 = [];
        var influencerData3 = [];
        query['registered'] = '1';
        query['status'] = '1';
        // category_filter['$in'] = ['C', 'D'];
        query['$or'] = [social_name, instagram_social_name, facebook_social_name]
        let skip = 0;
        let limit = 5;

        if(req.body.skip) {
            skip = req.body.skip
        }

        if(req.body.limit) {
            limit = req.body.limit
        }

        let page = req.body.page_number || 1;
        skip = (page-1)*limit;

        return influencerService.microInfluencerSearch(query, { 'twitter': 1, 'instagram': 1, 'facebook': 1, 'youtube': 1, "first_name": 1 }, skip, limit, (err, result) => {

            return Promise.all(result.map((influencerData,key) => {
                let influencerData1 = influencerData.toObject();

                for (let [key, value] of Object.entries(influencerData1)) {
                    if(value && value[0] && value[0]["Elv_social"]) {
                        if(value[0]["Elv_social"].toLowerCase().indexOf(req.body.keyword.toLowerCase()) == -1 ) {
                            delete influencerData1[key];
                        }
                    } else {
                        delete influencerData1[key];
                    }
                }

                // if(influencerData1['twitter'] && influencerData1['twitter'].indexOf() )
                var camp_inf_query = {};
                if(influencerData._id){
                    camp_inf_query.influencer_id = new ObjectId(influencerData._id)
                }
                if(req.body.campaign_id){
                    camp_inf_query.campaign_id = new ObjectId(req.body.campaign_id)
                }

                return campaignInfluencerModel.find(camp_inf_query)
                .then((ress) => {
                    ress.forEach(influencer => {
                        var platform = influencer.platform
                          if(influencerData1[platform] && influencerData1[platform][0]) {
                          influencerData1[platform][0]['campaign_details'] = {
                                                                                  action: influencer.action,
                                                                                  type: influencer.type,
                                                                                  platform_Elv_social: influencer.platform_Elv_social,
                                                                                  category: influencer.category,
                                                                                  cost: influencer.cost,
                                                                                  post_needed: influencer.post_needed,
                                                                                }
                        }

                    })
                    return influencerData1;
                });
            }))
            .then(alldata => {
                let list = [];
                alldata.map(obj => {
                    Object.keys(obj).map(key => {
                        obj[key].map(data => {
                            list.push({
                                ...data,
                                platform: key
                            })
                        })
                    })
                });

                influencer_model.countDocuments(query).then((infl_count) => {
                    if(infl_count>0){
                        pages = Math.round(infl_count/5);
                    }

                   // responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Micro Search Found!dsd',{list:list_data,page:page,pages:pages,limit: limit,total:infl_count});
                   responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Micro Search Found!',{list:list,limit: limit,total:infl_count,pages:pages,page:page});
                });


            })


        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Micro Search Found!dsd',result);

        })

    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

const addMicroInfluencer = (req, res) => {

        let check = helper.checkRequest(["campaign_id", "elv_social","platform"], req.body);
        if (check == true) {
            // var a = query for finding infulcer id on basis of plat social from influencers

            var social_eleve = req.body.elv_social;
            var platform = req.body.platform;
            var query = {};
            var query_key = platform+'.Elv_social';

            var query = {
                [query_key]: social_eleve
            }

            influencer_model.findOne(query,{_id:1}).then((influencer_data) => {
                let influencerId = influencer_data._id;
                let influencerDetails = {};
                influencerDetails = {
                    'campaign_id': ObjectId(req.body.campaign_id),
                    'influencer_id': influencerId,
                    'platform': req.body.platform,
                    'type': req.body.type,
                    'status': '1',
                    'action': '',
                    'Elv_social': social_eleve,
                    'category': req.body.category,
                    'cost': req.body.cost
                }

                campaignInfluencerService.createData(influencerDetails, (err, result) => {
                    if (err) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);

                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer Added!', result);

                    }
                });
            })
            .catch((err) => {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, "Influencer Not Found");
            })


    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);

    }
}

// Manage list KOL APi with searching and filters
const campaignKolsList = (req, res) => {
    let check = helper.checkRequest(["campaign_id", "admin_id"], req.body);
    if (check == true) {
        campaignService.findData({ '_id': new ObjectId(req.body.campaign_id) }, (error, result) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (result) {
                    let bodyData = {
                        campaign_id: new ObjectId(req.body.campaign_id),
                        search: req.body.search || "",
                        gender: (req.body.gender.length) ? { "$match": { "influencerData.gender": { $in: req.body.gender } } } : { "$match": {} },
                        status: (req.body.status) ? { "$match": { "action": req.body.status } } : { "$match": {} },
                        type: (req.body.type) ? { "$match": { "type": req.body.type } } : { "$match": {} },
                        social: (req.body.social) ? { "$match": { "platform": req.body.social } } : { "$match": {} },
                    }
                    campaignInfluencerService.manageInfluencerListWithPagiation(bodyData, (err, influencerInfo) => {
                        if (err) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                        }else{
                            if(influencerInfo.length < 1){
                                let camapignDetail = {
                                    "title" : result.title,
                                    "invitedPlatforms" : []
                                }
                                var data = {
                                    "influencerInfo":influencerInfo,
                                    "campaignDetail":camapignDetail,
                                    "page":req.body.page_number || 1,
                                    "limit":req.body.limit || 10,
                                    "pages":0,
                                    "total":0
                                }
                            } else {
                                var influencersIds = [];
                                var influencersList = [];
                                getSpreadedData = ({
                                    influencerData,
                                    ...rest
                                }) => ({
                                    influencerData,
                                    rest
                                });
                                handleProfileStatus = ({
                                    profile: { action, updated_at: updatedAt  },
                                    existingProfileStatus = ""
                                }) => {
                                    var updatedProfileStatus = "";
                                    var newProfileStatusInteger = convertStatusAction("label", action, "value");
                                    var existingProfileStatusInteger = convertStatusAction("label", existingProfileStatus, "value");
                                    if(existingProfileStatusInteger < newProfileStatusInteger){
                                        updatedProfileStatus =  convertStatusAction("value", newProfileStatusInteger, "label");
                                    }

                                    return updatedProfileStatus
                                        ? {
                                            action: updatedProfileStatus,
                                            updatedAt
                                        }
                                        : {
                                            action: existingProfileStatus,
                                            updatedAt
                                        };
                                };
                                influencerInfo.forEach(function(influ, index) {
                                    if (influ.influencerData.length > 0)
                                        influ.influencerData[0].type = influ.type;
                                    var influencerId = influ.influencer_id;
                                    if (influencersIds.length > 0 && influencersIds.indexOf(String(influencerId)) != -1) {
                                        let existingInfluencer = getExistingEnfluencer(influencerId, influencersList);
                                        existingInfluencer = {
                                            ...existingInfluencer,
                                            profiles: [
                                                ...existingInfluencer.profiles,
                                                {
                                                    ...getSpreadedData(influ).rest
                                                }
                                            ],
                                            profileStatus: {...handleProfileStatus({
                                                profile: {...getSpreadedData(influ).rest},
                                                existingProfileStatus: existingInfluencer.profileStatus.action,
                                            })},
                                        }
                                        influencersList.push(existingInfluencer);
                                    } else {
                                        var tempObj = {
                                            profileStatus: {...handleProfileStatus({
                                                profile: {...getSpreadedData(influ).rest},
                                                existingProfileStatus: "",
                                            })},
                                            influencer_id: influencerId,
                                            profiles: [{
                                                ...getSpreadedData(influ).rest
                                            }],
                                            influencerData: [...getSpreadedData(influ).influencerData],
                                        };
                                        influencersList.push(tempObj);
                                        influencersIds.push(String(influencerId));
                                    }
                                });
                                let page = req.body.page_number || 1;
                                let limit = req.body.limit || 10;
                                let data1 = influencersList.slice((page - 1) * limit, page * limit);

                                // Camapign Details
                                let invitedPlatforms = [];
                                result.platforms.forEach(platform => {
                                    const { name: platformName } = platform;
                                    if (invitedPlatforms.indexOf(platformName) === -1) {
                                        invitedPlatforms.push(platformName);
                                    }
                                });
                                let camapignDetail = {
                                    "title": result.title,
                                    "invitedPlatforms": invitedPlatforms
                                }
                                data = {
                                    "influencerInfo": data1,
                                    "campaignDetail":camapignDetail,
                                    "page": page,
                                    "total": influencersList.length,
                                    "limit": limit,
                                    "pages": Math.ceil(influencersList.length / limit)

                                }
                            }
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign Influencer Found!', data);
                        }
                    });
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Campaign not found!');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

//Influencer filter Function
getExistingEnfluencer = (id, influencersList) => {
    let influencerIndex = influencersList.findIndex(influencer =>
        String(influencer.influencer_id) === String(id)
    );
    return influencersList.splice(influencerIndex, 1)[0];
}

convertStatusAction = (keyToCheck, value, keyToFetch) => {
   return statusArr.find(obj => obj[keyToCheck] === value)[keyToFetch];
}


/* Export apis */
module.exports = {
    campaignPlatformListing: campaignPlatformListing,
    microSearch: microSearch,
    addMicroInfluencer: addMicroInfluencer,
    campaignKolsList
}