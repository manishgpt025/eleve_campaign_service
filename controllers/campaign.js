
const adminService = require('../services/adminService.js'); // admin services
const organisationService = require('../services/organisationServices.js'); // organisation services
const advertiserService = require('../services/advertiserService.js'); // advertiser services
const campaignService = require('../services/campaignServices.js'); //campaign services
const influencerService = require('../services/userServices.js'); // influencer services
const campaignInfluencerService = require('../services/campaignInfluencerServices.js'); // campaign influencer services
const helper = require('../globalFunctions/function.js'); // helper and glocal functions
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const ObjectId = require('mongodb').ObjectID;// Obj ID

/**
 * [manager List]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const managerList = (req, res) => {
    let check = helper.checkRequest(["admin_id"], req.body);
    if (check == true) {
        adminService.getData({ 'status': '1' }, (err, result) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else if (result.length > 0) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Manager List Found!', result);
            } else {
                responseHandle.sendResponseWithData(res, 403, 'Manager List Not Found!')
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [search Agency]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const searchAgency = (req, res) => {
    let check = helper.checkRequest(["type"], req.body);
    if (check == true) {
        organisationService.getAllData({
            'type': req.body.type,
            'is_asscoiated': '0'
        }, { 'type': 1, 'organisation_name': 1 }, (err, result) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else if (result.length > 0) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'List Found!', result);
            } else {
                responseHandle.sendResponseWithData(res, 403, 'List Not Found!')
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [search Advertiser]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const searchAdvertiser = (req, res) => {
    let check = helper.checkRequest(["organisation_id"], req.body);
    if (check == true) {
        advertiserService.getAllData({
            'organisation_id': req.body.organisation_id
        }, { 'name': 1, 'last_name': 1, 'email': 1, 'organisation_id': 1, 'associated_brands': 1 }, (err, result) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);

            } else if (result) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Advertiser Found!', result);
            } else {
                responseHandle.sendResponseWithData(res, 403, 'Advertiser Not Found!')

            }
        })
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [search Brand]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const searchBrand = (req, res) => {
    let check = helper.checkRequest(["associated_brands"], req.body);
    if (check == true) {
        organisationService.getAllData({
            '_id': { '$in': req.body.associated_brands }
        }, { 'organisation_name': 1 }, (err, result) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);

            } else if (result.length > 0) {
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Brand Found!', result);

            } else {
                responseHandle.sendResponseWithData(res, 403, 'Brand  Not Found!')

            }
        })
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [campaign Details By Id]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignDetailsById = (req, res) => {
    let check = helper.checkRequest(["campaign_id"], req.body);
    if (check == true) {
        campaignService.getData({ '_id': new ObjectId(req.body.campaign_id) }, (err, result) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
            } else if (result) {
                campaignInfluencerService.getAllData({
                    'campaign_id': req.body.campaign_id,
                    // "action": { '$ne': 'accepted' }
                }, (error, result_) => {
                    if (error) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else if (result_.length > 0) {
                        campaignInfluencerService.getAllData({
                            'campaign_id': req.body.campaign_id
                        }, (error_, resultCount) => {
                            if (error_) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                            } else {
                                campaignInfluencerService.findDataUnique({
                                    'campaign_id': req.body.campaign_id
                                }, (error1, uniqueInfluencerData) => {
                                    if (error1) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1);
                                    } else {
                                        let microInfluencersData = resultCount.filter(data => data.type == "micro");
                                        let premiumInfluencersData = resultCount.filter(data => data.type == "premium");
                                        let resultData = {
                                            "campaign": result,
                                            "campaign_influencers": result_,
                                            "total_influencers_count": resultCount.length,
                                            "total_influencers_count_unique": uniqueInfluencerData.length,
                                            "micro_influencers": microInfluencersData.length,
                                            "premium_influencers": premiumInfluencersData.length
                                        }
                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign & Influencers Found!', resultData);
                                    }
                                });
                            }
                        });
                    } else {
                        let resultData = {
                            "campaign": result,
                            "campaign_influencers": [],
                            "total_influencers_count": 0,
                            "total_influencers_count_unique": 0,
                            "micro_influencers": 0,
                            "premium_influencers": 0
                        }
                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign Found!', resultData);
                    }
                });
            } else {
                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Campaign Detail Not Found!')

            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [search Influencer]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const searchInfluencer = async (req, res) => {
    let check = helper.checkRequest(["platform"], req.body);
    if (check == true) {
       const platform = req.body.platform;
       let check_platform = helper.checkPlatform(platform);
        if(check_platform){
            responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Platform not found.');return;
        }
        let query = {};
        let follower_filter = {};
        let gender_filter = {};
        let age_filter = {};
        let category_filter = {};
        let campaign_influencer = [];
        let campaign_exclude_influencer = [];
        let final_result = {};
        let platform_filter = {};
        let min_reach_count = req.body.followers_less_than || 0;
        let max_reach_count =req.body.followers_more_than || 0;

        // Location filter
        if (req.body.country) {query['country'] = req.body.country;}
        if (req.body.state) { query['state'] = req.body.state;}
        if (req.body.city) {query['city'] = req.body.city;}

        // Reach count filter

        if (min_reach_count) {follower_filter['$gte'] = min_reach_count;}
        if (max_reach_count) { follower_filter['$lte'] = max_reach_count; }
        if (follower_filter.length > 0) { query[`${platform}.followers_count`] = follower_filter;}

        //Gender filter
        if ((req.body.gender.length > 0)) {
            gender_filter['$in'] = req.body.gender;
            query['gender'] = gender_filter;
        }
        //Age filter
        if (req.body.age.length > 0) {
            age_filter['$in'] = req.body.age;
            query['age_group'] = age_filter;
        }
        //Category filter
        if (req.body.category.length > 0) {
            category_filter['$in'] = req.body.category;
            query[`${platform}.category`] = category_filter;
        }

         if ((req.body.include_campaign).length > 0) {
            let influencerData  = await completedCampaignFind(req.body.include_campaign);
            if(influencerData.length){
                 campaign_influencer.push(influencerData);
            }
         }
        //Completed campaign exclude array filter
        if ((req.body.exclude_campaign).length > 0) {
           let influencerData  = await completedCampaignFind(req.body.include_campaign);
           if(influencerData.length){
               campaign_exclude_influencer.push(influencerData);
           }
        }
        platform_filter['$ne'] = null;
        query[platform] = platform_filter;
        query['registered'] = '1';
        query['status'] = '1';
        campaignInfluencerService.getAllData({
            'campaign_id': req.body.campaign_id,
            'action': 'accepted',
            'platform': platform,
            'type': req.body.type
        }, { 'influencer_id': 1 }, (error1, result1) => {
            if (error1) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1);
            } else if (result1.length > 0) {
                let influencer_not_accepted = [];
                let influencer_not_accepted_filter = {};
                result1.forEach(function(results) {
                    influencer_not_accepted.push(
                        results.influencer_id.toString()
                    );
                });
                if (influencer_not_accepted) {
                    influencer_not_accepted_filter['$nin'] = influencer_not_accepted;
                    query['_id'] = influencer_not_accepted_filter;
                }
                influencerService.getUserDetails(query, { 'first_name': 1, 'email': 1, [platform]: 1, 'last_name': 1 }, (err, result) => {
                    if (err) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                    } else if (result.length > 0) {
                        let influencers = [];
                        result.map((user, i) => {
                            let result_platform = user[platform];
                            let email = user.email;
                            let inf_id = user.id;
                            result_platform.map(results => {
                                if (req.body.category.length > 0 && (max_reach_count == "" || max_reach_count <= 0)) {
                                    if (req.body.category.indexOf(results.category) > -1) {
                                        influencers.push({
                                            'cost': results.cost,
                                            'category': results.category,
                                            'followers_count': results.followers_count,
                                            'Elv_social_id': results.id,
                                            'profile_image_url': results.profile_image_url ? results.profile_image_url:"",
                                            'Elv_social': results.Elv_social,
                                            'influencer_email': email,
                                            'influencer_id': inf_id
                                        });
                                    }
                                }
                                if ((req.body.category.length > 0) && max_reach_count) {
                                    if ((req.body.category.indexOf(results.category) > -1) && ((results.followers_count <= max_reach_count) && (results.followers_count >= min_reach_count))) {
                                        influencers.push({
                                            'cost': results.cost,
                                            'category': results.category,
                                            'followers_count': results.followers_count,
                                            'Elv_social_id': results.id,
                                            'profile_image_url': results.profile_image_url ? results.profile_image_url:"",
                                            'Elv_social': results.Elv_social,
                                            'influencer_email': email,
                                            'influencer_id': inf_id
                                        });
                                    }
                                }
                            });
                        });
                        final_result = influencers;
                        if(final_result.length)
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer Found!', final_result);
                        else
                            responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Influencer not Found!', final_result);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Influencer not Found!', final_result);
                    }
                });
            } else {
                influencerService.getUserDetails(query, { 'first_name': 1, 'email': 1, [platform]: 1, 'last_name': 1 }, (err, result) => {
                    if (err) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                    } else if (result.length > 0) {
                        let influencers = [];
                        result.map((user, i) => {
                            let result_platform = user[platform];
                            let email = user.email;
                            let inf_id = user.id;
                            result_platform.map(results => {
                                if (req.body.category.length > 0 && (max_reach_count == "" || max_reach_count <= 0)) {
                                    if (req.body.category.indexOf(results.category) > -1) {
                                        influencers.push({
                                            'cost': results.cost,
                                            'category': results.category,
                                            'followers_count': results.followers_count,
                                            'Elv_social_id': results.id,
                                            'profile_image_url': results.profile_image_url ? results.profile_image_url:"",
                                            'Elv_social': results.Elv_social,
                                            'influencer_email': email,
                                            'influencer_id': inf_id
                                        });
                                    }
                                }

                                if ((req.body.category.length > 0) && max_reach_count) {
                                    if ((req.body.category.indexOf(results.category) > -1) && ((results.followers_count <= max_reach_count) && (results.followers_count >= min_reach_count))) {
                                        influencers.push({
                                            'cost': results.cost,
                                            'category': results.category,
                                            'followers_count': results.followers_count,
                                            'Elv_social_id': results.id,
                                            'profile_image_url': results.profile_image_url ? results.profile_image_url:"",
                                            'Elv_social': results.Elv_social,
                                            'influencer_email': email,
                                            'influencer_id': inf_id
                                        });
                                    }
                                }
                            });
                        });
                        final_result = influencers;
                        if(final_result.length)
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer Found!', final_result);
                        else
                            responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Influencer not Found!', final_result);
                    } else {
                        responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Influencer not Found!', final_result);
                    }
                });
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [completed Campaign Find]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
completedCampaignFind = async (campaignIds) => {
    let result = '';
    result = await campaignInfluencerService.getAllDataAsync({'campaign_id': { '$in': campaignIds }},{influencer_id:1});
        if (result.length > 0) {
            return result;
        }
        return result;
}

/**
 * [campaign List]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignList = (req, res) => {
    let search_keyword = req.body.keyword ? req.body.keyword.toLowerCase(): "";
    let status_filter = req.body.status || "";
    let platform_filter = req.body.platform || "";

    // change status filter
    if (status_filter == "0") { //draft campaigns
        var filter = ['0', '5'];
    } else if (status_filter == "1") { // Active Campaigns
        var filter = ['2', '3', '4', '8'];
    } else if (status_filter == "2") {
        var filter = ['2'];
    } else if (status_filter == "3") {
        var filter = ['3'];
    }else if (status_filter == "4") {
        var filter = ['4'];
    }  else if (status_filter == "8") {
        var filter = ['8'];
    }else if (status_filter == "5") { //pending Campaigns
        var filter = ['5'];
    } else if (status_filter == "6") { // Completed Campaigns
        var filter = ['6'];
    } else {
        var filter = [];
    }
    // platform change status
    if (platform_filter == 'twitter')var plat_filter = ['twitter'];
    if (platform_filter == 'instagram')var plat_filter = ['instagram'];
    if (platform_filter == 'facebook')var plat_filter = ['facebook'];
    if (platform_filter == 'youtube')var plat_filter = ['youtube'];
    if (platform_filter == '') var plat_filter = [];
    let bodyData = {
        search: search_keyword ,
        status: (filter.length) ? {"$match": { "status": { $in: filter } } } : { "$match":{} },
        platform: (plat_filter.length) ? {"$match": { "platforms.name": { $in: plat_filter } } } : { "$match":{} }
    }
    let options = {
        page: req.body.pageNumber || 1,
        limit: req.body.limit || 10,
    }
    var sort = { $sort: { "_id": -1 }};
    campaignService.getPaginateDataWithAggregate(bodyData, options, sort, (error, campaignInfo, pages, total) => {
        if (error) {
            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
        } else {
            if (campaignInfo) {
                let data = {
                    docs: campaignInfo,
                    page: req.body.pageNumber || 1,
                    limit: req.body.limit || 10,
                    pages: pages,
                    total: total
                }
                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign List!', data);
            } else {
                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Data not found!');
            }
        }
    });
}

/* Export apis */
module.exports = {
    managerList,
    searchAgency,
    searchAdvertiser,
    searchBrand,
    campaignDetailsById,
    searchInfluencer,
    campaignList
}