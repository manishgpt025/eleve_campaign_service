const request = require('request-promise');
// Obj ID
const ObjectId = require('mongodb').ObjectID;
//use helper
const helper = require('../globalFunctions/function.js');
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const responseMessage = require('../globalFunctions/httpResponseMessage.js');

/**
 * [Campaign List by User Id]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignList = (req, res) => {
    let check = helper.checkRequest(["id"], req.params);
    if (check == true) {

    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/*Export apis*/
module.exports = {
    campaignList: campaignList
}