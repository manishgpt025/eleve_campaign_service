const campaignService = require('../services/campaignServices.js');// Campaign services
const campaignInfluencerService = require('../services/campaignInfluencerServices.js');// Campaign Influencer services
const adminService = require('../services/adminService.js');// admin services
const helper = require('../globalFunctions/function.js');// helper and glocal functions
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const notification = require('../globalFunctions/mail_sms_function.js');
const emailTemplate = require('../emailTemplate/campaign_email.js');
const campaignController = require("../controllers/campaignController");
const ObjectId = require('mongodb').ObjectID; // Obj ID
const _ = require('lodash');

/**
 * [create campaign step one]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const create_campaign_step_one = (req, res) => {
    let check = helper.checkRequest(["title"], req.body);
    if (check == true) {
        if (!req.headers.campaign_id || req.headers.campaign_id == "" || req.headers.campaign_id == undefined) {
            let newId = helper.generateRandomString();
            req.body.campaign_id = newId;
            campaignService.createData(req.body, (error, result_) => {
                if (error) {
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Create campaign step one created successfully.', result_);
                }
            });
        } else {
            let myquery = {
                _id: new ObjectId(req.headers.campaign_id)
            };
            campaignService.findData(myquery, (err, result) => {
                if(err){
                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                }else  if(!result){
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Campaign not exist.');
                }else{
                    let updateData = req.body;
                    if(updateData.type == 'amplification'){
                        updateData.images = [];
                    }else{
                        result.images.forEach(function(item) {
                            updateData.images.push({ "url": item.url })
                        });
                    }
                    let newvalues = {
                        $set: updateData
                    };
                    if(result.status == '3')
                        campaignUpdateBasicDeatilsNotification(result, updateData, 'one');

                    campaignService.updateOne(myquery, newvalues, {
                        new: true
                    }, (error, result_) => {
                        if (error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign step one updated successfully.', result_);
                        }
                    });
                }

            });
        }
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [create campaign step two b]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const create_campaign_step_two_b = (req, res) => {
    let check = helper.checkRequest(["campaign_id", "platform", "type", "influencers"], req.body);
    if (check == true) {
        let myquery = {
            _id: new ObjectId(req.body.campaign_id)
        };
        campaignService.findData(myquery, async (err, resultC) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (req.body.influencers.length > 0) {
                    var influencerData = [];
                    req.body.influencers.forEach(function(data) {
                        let newId = helper.generateRandomString();
                        influencerData.push({
                            "campaign_influencer_id":newId,
                            "campaign_id": req.body.campaign_id,
                            "influencer_id": data.influencer_id,
                            "platform": req.body.platform,
                            "type": req.body.type,
                            "Elv_social": data.Elv_social,
                            "category": data.category,
                            "profile_image_url": data.profile_image_url ? data.profile_image_url:"",
                            "cost": data.cost ? data.cost: '0',
                            "post_needed": req.body.postPerKOL,
                            "action":"",
                            "post_done":0,
                            "status":"1"
                        });
                    });
                    if ((req.body.platform_id == "" || req.body.platform_id == undefined) && resultC.status !== '3') {
                        campaignInfluencerService.createData(influencerData, (error, result_) => {
                            if (error) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                            } else {
                                let platformData = {
                                    "name": req.body.platform,
                                    "type": req.body.type,
                                    "number_influencer": req.body.maxInfluencer,
                                    "number_post": req.body.postPerKOL,
                                    "number_conversation": req.body.conversations
                                }
                                let influencerData = {
                                    "name": req.body.platform,
                                    "type": req.body.type,
                                    "count": req.body.maxInfluencer
                                }
                                let postData = {
                                    "name": req.body.platform,
                                    "type": req.body.type,
                                    "count": req.body.postPerKOL
                                }
                                let conversationData = {
                                    "name": req.body.platform,
                                    "type": req.body.type,
                                    "count": req.body.conversations
                                }
                                let mentionData = {
                                    "platform": req.body.platform,
                                    "name": req.body.mentions
                                }
                                if(req.body.genderspecific === "0"){
                                    var commonData = {
                                        "platform": req.body.platform,
                                        "description": req.body.commonDescription
                                    }
                                }else{
                                    var commonData = [];
                                }

                                if(req.body.genderspecific === "1"){
                                    var maleData = {
                                        "platform": req.body.platform,
                                        "description": req.body.maleDescription
                                    }
                                }else{
                                    var maleData = [];
                                }
                                if(req.body.genderspecific  === "1"){
                                    var femaleData = {
                                        "platform": req.body.platform,
                                        "description": req.body.femaleDescription
                                    }
                                }else{
                                    var femaleData = [];
                                }
                                if(req.body.genderspecific === "1"){
                                    var otherData = {
                                        "platform": req.body.platform,
                                        "description": req.body.otherDescription
                                    }
                                }else{
                                    var otherData = [];
                                }
                                if(req.body.genderspecific === "0"){
                                    var commonSampleData = {
                                        "platform": req.body.platform,
                                        "sample": req.body.commonSample
                                    }
                                }else{
                                    var commonSampleData = [];
                                }
                                if(req.body.genderspecific === "1"){
                                    var maleCommomData = {
                                        "platform": req.body.platform,
                                        "sample": req.body.maleSample
                                    }
                                }else{
                                    var maleCommomData = []
                                }
                                if(req.body.genderspecific === "1"){
                                    var femaleCommomData = {
                                        "platform": req.body.platform,
                                        "sample": req.body.femaleSample
                                    }
                                }else{
                                    var femaleCommomData = []
                                }
                                if(req.body.genderspecific === "1"){
                                    var otherCommomData = {
                                        "platform": req.body.platform,
                                        "sample": req.body.otherSample
                                    }
                                }else{
                                    var otherCommomData = [];
                                }

                                var gender_specific = {
                                    "platform": req.body.platform,
                                    "is_gender": req.body.genderspecific
                                }

                                let updateData = {
                                    platforms: platformData,
                                    number_influencer: influencerData,
                                    number_post: postData,
                                    conversations: conversationData,
                                    mentions: mentionData,
                                    platform_specific: gender_specific,
                                    description: commonData,
                                    male_description: maleData,
                                    female_description: femaleData,
                                    other_description: otherData,
                                    sample: commonSampleData,
                                    male_sample: maleCommomData,
                                    female_sample: femaleCommomData,
                                    other_sample: otherCommomData
                                }
                                let newvalues = {
                                    $push: updateData,
                                    $set: {'status':req.body.status}
                                };
                                campaignService.arrayUpdate(myquery, newvalues, {
                                    new: true
                                }, (error3, resp) => {
                                    if (error3) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                    } else {
                                        if(req.body.copyBriefDescription != "" || (resultC.micro_common_brief == req.body.platform)){
                                            let updateData = {
                                                'micro_common_brief' : req.body.copyBriefDescription
                                            }
                                            let newvalues = {
                                                $set: updateData
                                            };
                                            campaignService.updateOne(myquery, newvalues, {
                                                new: true
                                            }, (error_, result_) => {
                                                if (error_) {
                                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                                                } else {
                                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Platform created successfully.', result_);
                                                }
                                            });
                                        }else{
                                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Platform created successfully.', resp);
                                        }
                                    }
                                });
                            }
                        });
                    } else {
                        let resultData = await campaignController.campaignPlatformDeleteFunction(resultC.status, req.body.campaign_id,req.body.platform_id,req.body.platform,req.body.type );
                        if(resultData){
                            if(resultC.status && resultC.status === '3'){
                                let condition = {
                                    campaign_id: new ObjectId(req.body.campaign_id),
                                    platform: req.body.platform,
                                    type: req.body.type
                                }
                                campaignInfluencerService.getAllData(condition,{},(errorLive, resultLive) => {
                                    if (errorLive) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, errorLive);
                                    }else{
                                        resultLive = resultLive.filter(obj => obj.action != "");
                                        resultLive = resultLive.map(obj => {
                                            let { _id, ...restData } = obj;

                                            return {...restData, post_needed:req.body.postPerKOL};
                                        });
                                        let influencerNewData = [];
                                        let influencerFinalData = [];
                                        influencerData.forEach(influencer => {
                                            let liveIndex = resultLive.findIndex(live => live.Elv_social === influencer.Elv_social);
                                                if(liveIndex === -1) {
                                                    influencerNewData.push(influencer);
                                                }
                                        });
                                        influencerFinalData = [...influencerNewData, ...resultLive];
                                            campaignInfluencerService.removeRecord(condition, { new: true}, (error_, results_) => {
                                                if (error_) {
                                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                                                } else {
                                                    campaignInfluencerService.createData(influencerFinalData, (error, result_) => {
                                                        if (error) {
                                                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                                        } else {
                                                            let platformData = {
                                                                "name": req.body.platform,
                                                                "type": req.body.type,
                                                                "number_influencer": req.body.maxInfluencer,
                                                                "number_post": req.body.postPerKOL,
                                                                "number_conversation": req.body.conversations
                                                            }
                                                            let influencerData = {
                                                                "name": req.body.platform,
                                                                "type": req.body.type,
                                                                "count": req.body.maxInfluencer
                                                            }
                                                            let postData = {
                                                                "name": req.body.platform,
                                                                "type": req.body.type,
                                                                "count": req.body.postPerKOL
                                                            }
                                                            let conversationData = {
                                                                "name": req.body.platform,
                                                                "type": req.body.type,
                                                                "count": req.body.conversations
                                                            }
                                                            let mentionData = {
                                                                "platform": req.body.platform,
                                                                "name": req.body.mentions
                                                            }
                                                            if(req.body.genderspecific === "0"){
                                                                var commonData = {
                                                                    "platform": req.body.platform,
                                                                    "description": req.body.commonDescription
                                                                }
                                                            }else{
                                                                var commonData = [];
                                                            }

                                                            if(req.body.genderspecific === "1"){
                                                                var maleData = {
                                                                    "platform": req.body.platform,
                                                                    "description": req.body.maleDescription
                                                                }
                                                            }else{
                                                                var maleData = [];
                                                            }
                                                            if(req.body.genderspecific  === "1"){
                                                                var femaleData = {
                                                                    "platform": req.body.platform,
                                                                    "description": req.body.femaleDescription
                                                                }
                                                            }else{
                                                                var femaleData = [];
                                                            }
                                                            if(req.body.genderspecific === "1"){
                                                                var otherData = {
                                                                    "platform": req.body.platform,
                                                                    "description": req.body.otherDescription
                                                                }
                                                            }else{
                                                                var otherData = [];
                                                            }
                                                            if(req.body.genderspecific === "0"){
                                                                var commonSampleData = {
                                                                    "platform": req.body.platform,
                                                                    "sample": req.body.commonSample
                                                                }
                                                            }else{
                                                                var commonSampleData = [];
                                                            }
                                                            if(req.body.genderspecific === "1"){
                                                                var maleCommomData = {
                                                                    "platform": req.body.platform,
                                                                    "sample": req.body.maleSample
                                                                }
                                                            }else{
                                                                var maleCommomData = []
                                                            }
                                                            if(req.body.genderspecific === "1"){
                                                                var femaleCommomData = {
                                                                    "platform": req.body.platform,
                                                                    "sample": req.body.femaleSample
                                                                }
                                                            }else{
                                                                var femaleCommomData = []
                                                            }
                                                            if(req.body.genderspecific === "1"){
                                                                var otherCommomData = {
                                                                    "platform": req.body.platform,
                                                                    "sample": req.body.otherSample
                                                                }
                                                            }else{
                                                                var otherCommomData = [];
                                                            }

                                                            var gender_specific = {
                                                                "platform": req.body.platform,
                                                                "is_gender": req.body.genderspecific
                                                            }

                                                            let updateData = {
                                                                platforms: platformData,
                                                                number_influencer: influencerData,
                                                                number_post: postData,
                                                                conversations: conversationData,
                                                                mentions: mentionData,
                                                                platform_specific: gender_specific,
                                                                description: commonData,
                                                                male_description: maleData,
                                                                female_description: femaleData,
                                                                other_description: otherData,
                                                                sample: commonSampleData,
                                                                male_sample: maleCommomData,
                                                                female_sample: femaleCommomData,
                                                                other_sample: otherCommomData
                                                            }
                                                            let newvalues = {
                                                                $push: updateData
                                                            };
                                                            campaignService.arrayUpdate(myquery, newvalues, {
                                                                new: true
                                                            }, (error3, resp) => {
                                                                if (error3) {
                                                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                                                } else {
                                                                    campaignMicroDeatilsNotification(resultC, req.body,influencerNewData);
                                                                    if(req.body.copyBriefDescription != "" || (resultC.micro_common_brief == req.body.platform)){
                                                                        let updateData = {'micro_common_brief' : req.body.copyBriefDescription}
                                                                        let newvalues = {
                                                                            $set: updateData
                                                                        };
                                                                        campaignService.updateOne(myquery, newvalues, {
                                                                            new: true
                                                                        }, (error_, result_) => {
                                                                            if (error_) {
                                                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                                                                            } else {
                                                                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Platform updated successfully.', result_);
                                                                            }
                                                                        });
                                                                    }else{
                                                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Platform updated successfully.', resp);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                    }
                                });
                                return;
                            }else{
                                let condition = {
                                    campaign_id: new ObjectId(req.body.campaign_id),
                                    platform: req.body.platform,
                                    type: req.body.type
                                }
                                campaignInfluencerService.removeRecord(condition, {
                                    new: true
                                }, (error_, result_) => {
                                    if (error_) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                                    } else {
                                        campaignInfluencerService.createData(influencerData, (error, result_) => {
                                            if (error) {
                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                            } else {
                                                let platformData = {
                                                    "name": req.body.platform,
                                                    "type": req.body.type,
                                                    "number_influencer": req.body.maxInfluencer,
                                                    "number_post": req.body.postPerKOL,
                                                    "number_conversation": req.body.conversations
                                                }
                                                let influencerData = {
                                                    "name": req.body.platform,
                                                    "type": req.body.type,
                                                    "count": req.body.maxInfluencer
                                                }
                                                let postData = {
                                                    "name": req.body.platform,
                                                    "type": req.body.type,
                                                    "count": req.body.postPerKOL
                                                }
                                                let conversationData = {
                                                    "name": req.body.platform,
                                                    "type": req.body.type,
                                                    "count": req.body.conversations
                                                }
                                                let mentionData = {
                                                    "platform": req.body.platform,
                                                    "name": req.body.mentions
                                                }
                                                if(req.body.genderspecific === "0"){
                                                    var commonData = {
                                                        "platform": req.body.platform,
                                                        "description": req.body.commonDescription
                                                    }
                                                }else{
                                                    var commonData = [];
                                                }

                                                if(req.body.genderspecific === "1"){
                                                    var maleData = {
                                                        "platform": req.body.platform,
                                                        "description": req.body.maleDescription
                                                    }
                                                }else{
                                                    var maleData = [];
                                                }
                                                if(req.body.genderspecific  === "1"){
                                                    var femaleData = {
                                                        "platform": req.body.platform,
                                                        "description": req.body.femaleDescription
                                                    }
                                                }else{
                                                    var femaleData = [];
                                                }
                                                if(req.body.genderspecific === "1"){
                                                    var otherData = {
                                                        "platform": req.body.platform,
                                                        "description": req.body.otherDescription
                                                    }
                                                }else{
                                                    var otherData = [];
                                                }
                                                if(req.body.genderspecific === "0"){
                                                    var commonSampleData = {
                                                        "platform": req.body.platform,
                                                        "sample": req.body.commonSample
                                                    }
                                                }else{
                                                    var commonSampleData = [];
                                                }
                                                if(req.body.genderspecific === "1"){
                                                    var maleCommomData = {
                                                        "platform": req.body.platform,
                                                        "sample": req.body.maleSample
                                                    }
                                                }else{
                                                    var maleCommomData = []
                                                }
                                                if(req.body.genderspecific === "1"){
                                                    var femaleCommomData = {
                                                        "platform": req.body.platform,
                                                        "sample": req.body.femaleSample
                                                    }
                                                }else{
                                                    var femaleCommomData = []
                                                }
                                                if(req.body.genderspecific === "1"){
                                                    var otherCommomData = {
                                                        "platform": req.body.platform,
                                                        "sample": req.body.otherSample
                                                    }
                                                }else{
                                                    var otherCommomData = [];
                                                }

                                                var gender_specific = {
                                                    "platform": req.body.platform,
                                                    "is_gender": req.body.genderspecific
                                                }

                                                let updateData = {
                                                    platforms: platformData,
                                                    number_influencer: influencerData,
                                                    number_post: postData,
                                                    conversations: conversationData,
                                                    mentions: mentionData,
                                                    platform_specific: gender_specific,
                                                    description: commonData,
                                                    male_description: maleData,
                                                    female_description: femaleData,
                                                    other_description: otherData,
                                                    sample: commonSampleData,
                                                    male_sample: maleCommomData,
                                                    female_sample: femaleCommomData,
                                                    other_sample: otherCommomData
                                                }
                                                let newvalues = {
                                                    $push: updateData,
                                                    $set: {'status':req.body.status}
                                                };
                                                campaignService.arrayUpdate(myquery, newvalues, {
                                                    new: true
                                                }, (error3, resp) => {
                                                    if (error3) {
                                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                                    } else {
                                                        if(req.body.copyBriefDescription != "" || (resultC.micro_common_brief == req.body.platform)){
                                                            let updateData = {
                                                                'micro_common_brief' : req.body.copyBriefDescription,
                                                            }
                                                            let newvalues = {
                                                                $set: updateData
                                                            };
                                                            campaignService.updateOne(myquery, newvalues, {
                                                                new: true
                                                            }, (error_, result_) => {
                                                                if (error_) {
                                                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                                                                } else {
                                                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Platform updated successfully.', result_);
                                                                }
                                                            });
                                                        }else{
                                                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Platform updated successfully.', resp);
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Please provide influencers.');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);

    }
}

/**
 * [create campaign step two c]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const create_campaign_step_two_c = (req, res) => {
    let check = helper.checkRequest(["campaign_id", "platform", "type"], req.body);
    if (check == true) {
        let myquery = {
            _id: new ObjectId(req.body.campaign_id)
        };
        campaignService.findData(myquery, async(err, resultC) => {
            if (err) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (req.body.influencers.length > 0 || resultC.status === '3') {
                    var influencerData = [];
                    if(req.body.influencers.length > 0){
                        req.body.influencers.forEach(function(data) {
                            let newId = helper.generateRandomString();
                            influencerData.push({
                                "campaign_influencer_id":newId,
                                "campaign_id": req.body.campaign_id,
                                "influencer_id": data.influencer_id,
                                "platform": req.body.platform,
                                "type": req.body.type,
                                "Elv_social": data.Elv_social,
                                "category": data.category,
                                "profile_image_url": data.profile_image_url ? data.profile_image_url:"",
                                "currency_code":data.currency_code,
                                "cost": data.cost,
                                "package_cost": data.package_cost,
                                "post_needed": data.post_count,
                                "premium_brief": data.brief,
                                "premium_sample": data.sample ? data.sample : "",
                                "action":"",
                                "post_done":0,
                                "status":"1"
                            });
                        });
                    }
                    if ((req.body.platform_id == "" || req.body.platform_id == undefined) && resultC.status !== '3') {
                        campaignInfluencerService.createData(influencerData, (error, result_) => {
                            if (error) {
                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                            } else {
                                let platformData = {
                                    "name": req.body.platform,
                                    "type": req.body.type,
                                    "number_influencer": req.body.influencerCount,
                                    "number_post": req.body.postCount,
                                    "number_conversation": req.body.conversationCount
                                }
                                let influencerData = {
                                    "name": req.body.platform,
                                    "type": req.body.type,
                                    "count": req.body.influencerCount
                                }
                                let postData = {
                                    "name": req.body.platform,
                                    "type": req.body.type,
                                    "count": req.body.postCount
                                }
                                let conversationData = {
                                    "name": req.body.platform,
                                    "type": req.body.type,
                                    "count": req.body.conversationCount
                                }
                                let breif_to_all = {
                                    "platform": req.body.platform,
                                    "status": req.body.breifToAll
                                }
                                let updateData = {
                                    platforms: platformData,
                                    number_influencer: influencerData,
                                    number_post: postData,
                                    conversations: conversationData,
                                    premium_common_brief: breif_to_all
                                }
                                let newvalues = {
                                    $push: updateData,
                                    $set: {'status':req.body.status}
                                };
                                campaignService.arrayUpdate(myquery, newvalues, {
                                    new: true
                                }, (error3, resp) => {
                                    if (error3) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                    } else {
                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Premium platform created successfully.', resp);
                                    }
                                });
                            }
                        });
                    } else {
                        let resultData = await campaignController.campaignPlatformDeleteFunction(resultC.status, req.body.campaign_id,req.body.platform_id,req.body.platform,req.body.type );
                        if(resultData){
                            if(resultC && resultC.status === '3'){
                                let condition = {
                                    campaign_id: new ObjectId(req.body.campaign_id),
                                    platform: req.body.platform,
                                    type: req.body.type
                                }
                                campaignInfluencerService.getAllData(condition,{},(errorLive, resultLive) => {
                                    if(errorLive) responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, errorLive);
                                    else{
                                        let changeInfluencerData = [];
                                        let newInviteInfluencers = [];
                                        const updateInfluencerData = influencerData.map(influencer => {
                                            let currentLiveObj = resultLive.find(live => live.Elv_social === influencer.Elv_social);
                                                if(currentLiveObj) {
                                                    const {action,post_done,post_needed:livePostNeeded,premium_brief:livePremiumBrief} = currentLiveObj;
                                                    const {post_needed:newPostNeeded,premium_brief:newPremiumBrief,influencer_id} = influencer;
                                                    let temp = [];
                                                    if(livePostNeeded != newPostNeeded)
                                                        temp.push('Post Count');
                                                    if(livePremiumBrief != newPremiumBrief)
                                                        temp.push('Campaign Brief');
                                                    if(temp.length)
                                                        changeInfluencerData.push({
                                                            [influencer_id]:temp
                                                        });

                                                     return {
                                                         ...influencer,
                                                         action,
                                                         post_done
                                                     }
                                                }else{
                                                    newInviteInfluencers.push(influencer.influencer_id);
                                                    return influencer;
                                                }
                                        });
                                        campaignPremiumDeatilsNotification(resultC, changeInfluencerData, newInviteInfluencers);
                                        campaignInfluencerService.removeRecord(condition, { new: true}, (errorRemove, resultsRemove) => {
                                            if(errorRemove) responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, errorRemove);
                                            else{
                                                campaignInfluencerService.createData(updateInfluencerData, (error, result_) => {
                                                    if (error) {
                                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                                    } else {
                                                        let platformData = {
                                                            "name": req.body.platform,
                                                            "type": req.body.type,
                                                            "number_influencer": req.body.influencerCount,
                                                            "number_post": req.body.postCount,
                                                            "number_conversation": req.body.conversationCount
                                                        }
                                                        let influencerData = {
                                                            "name": req.body.platform,
                                                            "type": req.body.type,
                                                            "count": req.body.influencerCount
                                                        }
                                                        let postData = {
                                                            "name": req.body.platform,
                                                            "type": req.body.type,
                                                            "count": req.body.postCount
                                                        }
                                                        let conversationData = {
                                                            "name": req.body.platform,
                                                            "type": req.body.type,
                                                            "count": req.body.conversationCount
                                                        }


                                                        let breif_to_all = {
                                                            "platform": req.body.platform,
                                                            "status": req.body.breifToAll
                                                        }

                                                        let updateData = {
                                                            platforms: platformData,
                                                            number_influencer: influencerData,
                                                            number_post: postData,
                                                            conversations: conversationData,
                                                            premium_common_brief: breif_to_all
                                                        }
                                                        let newvalues = {
                                                            $push: updateData
                                                        };
                                                        campaignService.arrayUpdate(myquery, newvalues, {
                                                            new: true
                                                        }, (error3, resp) => {
                                                            if (error3) {
                                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                                            } else {
                                                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Premium platform updated successfully.', resp);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });

                            }else{
                                let condition = {
                                    campaign_id: new ObjectId(req.body.campaign_id),
                                    platform: req.body.platform,
                                    type: req.body.type
                                }
                                campaignInfluencerService.removeRecord(condition, {
                                    new: true
                                }, (error_, result_) => {
                                    if (error_) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                                    } else {
                                        campaignInfluencerService.createData(influencerData, (error, result_) => {
                                            if (error) {
                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                            } else {
                                                let platformData = {
                                                    "name": req.body.platform,
                                                    "type": req.body.type,
                                                    "number_influencer": req.body.influencerCount,
                                                    "number_post": req.body.postCount,
                                                    "number_conversation": req.body.conversationCount
                                                }
                                                let influencerData = {
                                                    "name": req.body.platform,
                                                    "type": req.body.type,
                                                    "count": req.body.influencerCount
                                                }
                                                let postData = {
                                                    "name": req.body.platform,
                                                    "type": req.body.type,
                                                    "count": req.body.postCount
                                                }
                                                let conversationData = {
                                                    "name": req.body.platform,
                                                    "type": req.body.type,
                                                    "count": req.body.conversationCount
                                                }

                                                let breif_to_all = {
                                                    "platform": req.body.platform,
                                                    "status": req.body.breifToAll
                                                }

                                                let updateData = {
                                                    platforms: platformData,
                                                    number_influencer: influencerData,
                                                    number_post: postData,
                                                    conversations: conversationData,
                                                    premium_common_brief: breif_to_all
                                                }
                                                let newvalues = {
                                                    $push: updateData,
                                                    $set: {'status':req.body.status}
                                                };
                                                campaignService.arrayUpdate(myquery, newvalues, {
                                                    new: true
                                                }, (error3, resp) => {
                                                    if (error3) {
                                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                                    } else {
                                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Premium platform updated successfully.', resp);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Please provide influencers.');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [create campaign step three]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const create_campaign_step_three = (req, res) => {
    let check = helper.checkRequest(["campaign_id"], req.body);
    if (check == true) {
        let created_data = req.body.created_detail.email;
        let checkQuery = {
            email: created_data
        };
        adminService.findData(checkQuery,(errCreater, resultCreater)=> {
            if(errCreater) responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, errCreater);
            else if(resultCreater){
                let myquery = {
                    _id: new ObjectId(req.body.campaign_id)
                };
                campaignService.findData(myquery, (err, result) => {
                    if (err) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                    } else {
                        if(!result){
                            responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Campaign not found.');
                        }else if(result && result.status == "3"){
                            let updateData = {
                                'end_date': req.body.endDate,
                            }
                            let newvalues = {
                                $set: updateData
                            };
                            campaignUpdateNotification(result, req.body, 'three');

                            campaignService.arrayUpdate(myquery, newvalues, {
                                new: true
                            }, (error, resp) => {
                                if (error) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                } else {
                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign updated successfully', resp);
                                }
                            });
                        }else{
                            let updateData = {
                                'start_date': req.body.startDate,
                                'end_date': req.body.endDate,
                                'status': req.body.status,
                                'is_invited': req.body.invitationStatus,
                                'send_micro': req.body.invitationStatus ? req.body.sendMicro :'0',
                                'send_premium': req.body.invitationStatus ? req.body.sendPremium :'0',
                                'send_email': req.body.invitationStatus ?  req.body.sendEmail:'0',
                                'email_sender': req.body.invitationStatus ?  req.body.emailSender:'',
                                'email_subject': req.body.invitationStatus ?  req.body.emailSubject :'',
                                'reply_to': req.body.invitationStatus ?  req.body.replyTo :[],
                                'email_body': req.body.invitationStatus ?  req.body.emailBody:'',
                                'send_sms': req.body.invitationStatus ?  req.body.sendSms :'0',
                                'sms_body': req.body.invitationStatus ?  req.body.smsBody : '',
                                'created_by': (resultCreater) ? resultCreater.name : '',
                                'created_id': (resultCreater) ? resultCreater._id : ''
                            }
                            let newvalues = {
                                $set: updateData
                            };
                            campaignService.arrayUpdate(myquery, newvalues, {
                                new: true
                            }, (error, resp) => {
                                if (error) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                } else {
                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign created successfully', resp);
                                }
                            });
                        }


                    }
                });
            }else{
                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Invaild request.');
            }
        });


    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Campaign notificaton for update basic details]
*/
campaignUpdateBasicDeatilsNotification = (dbData, newData,step) => {
    let updateData = []
    //For step one update
    if(step === 'one'){
        if(!_.isEqual(dbData.secondary_hashtag, newData.secondary_hashtag))
        updateData.push('Secondary Hashtag');
    if(!_.isEqual(dbData.url, newData.url))
        updateData.push('URL to be shared');
    if(!_.isEqual(dbData.keywords, newData.keywords))
        updateData.push('Keywords to mention');
    if(!_.isEqual(dbData.retweet_url, newData.retweet_url))
        updateData.push('RT URL');
    let dbImages = dbData.images.map(obj => {
        const {_id,url} = obj;
        return { url };
    });
    if(!_.isEqual(dbImages, newData.images))
        updateData.push('Media');
    if(!_.isEqual(dbData.video, newData.video))
        updateData.push('Media');
    }
    //For step three update
    if(step === 'three'){
        if(!_.isEqual(dbData.end_date, newData.end_date))
        updateData.push('End Date');
    }

    updateData = [...new Set(updateData)];
    if(updateData.length > 0){
        let campaignId = dbData._id;
        let invitedstatus  = dbData.is_invited;
        let microstatus  = dbData.send_micro;
        let premiumstatus  = dbData.send_premium;
        let emailStatus = dbData.send_email;
        let smsstatus = dbData.send_sms;
        let emailSender = dbData.email_sender;

        let emailSubject = 'Campaign you accepted has been updated | Eleve';
        let emailBody = emailTemplate.update_basic_details_email_html(updateData);
        let smsBody = emailTemplate.update_basic_details_sms_content();
        notification.update_basic_detail_campaign_notification(campaignId , 'micro', emailStatus, smsstatus, emailSender, emailBody, emailSubject, smsBody);

    // 	addInfluencers(campaignId , 'premium', emailStatus, smsstatus, emailSender, emailBody, emailSubject, smsBody);
    }
}

/**
 * [Campaign notificaton for update micro section details]
*/
campaignMicroDeatilsNotification = (dbData, newData,newInfluencerData) => {
    let updateData = []
    let platformData = dbData.platforms.filter(obj => (obj.name == newData.platform && obj.type == 'micro'));
    if(platformData.length){
        const [{number_post:post_count}] = platformData || [{number_post:''}];
        if(post_count != newData.postPerKOL)
            updateData.push('Post Count');

        let mentionData = dbData.mentions.filter(obj => obj.platform == newData.platform);
        const [{name:mention_name}] = mentionData || [{name:[]}];

        if(!_.isEqual(mention_name, newData.mentions))
            updateData.push('Handles to mention');
        let platform_specific_data = dbData.platform_specific.filter(obj => obj.platform == newData.platform);
        const [{is_gender:gender_specific}] = platform_specific_data || [{is_gender:''}];
        if(gender_specific != newData.genderspecific){
            updateData.push('Campaign Brief');
        }else{
            if(newData.genderspecific == '0'){
                let descriptionData = dbData.description.filter(obj => obj.platform == newData.platform);
                const [{description:descriptionText}] = descriptionData || [{description:''}];
                if(!_.isEqual(descriptionText, newData.commonDescription))
                    updateData.push('Campaign Brief');
            }
            if(newData.genderspecific == '1'){
                let descriptionMaleData = dbData.male_description.filter(obj => obj.platform == newData.platform);
                const [{description:descriptionText}] = descriptionMaleData || [{description:''}];
                if(!_.isEqual(descriptionText, newData.maleDescription))
                    updateData.push('Campaign Brief');

                let descriptionfemaleData = dbData.female_description.filter(obj => obj.platform == newData.platform);
                const [{description:descriptionFemaleText}] = descriptionfemaleData || [{description:''}];
                if(!_.isEqual(descriptionFemaleText, newData.femaleDescription))
                    updateData.push('Campaign Brief');

                let descriptionOtherData = dbData.other_description.filter(obj => obj.platform == newData.platform);
                const [{description:descriptionOtherText}] = descriptionOtherData || [{description:''}];
                if(!_.isEqual(descriptionOtherText, newData.otherDescription))
                    updateData.push('Campaign Brief');
            }
        }
        updateData = [...new Set(updateData)];
    }
    let campaignId = dbData._id;
    let emailStatus = dbData.send_email;
    let smsstatus = dbData.send_sms;
    let emailBody = dbData.email_body;
    let emailSender = dbData.email_sender;
    let emailSubject   = dbData.email_subject;
    let smsBody  = dbData.sms_body;
    if(updateData.length > 0){
        let emailSubjectUpdate = 'Campaign you accepted has been updated | Eleve';
        let emailBodyUpdate = emailTemplate.update_basic_details_email_html(updateData);
        let smsBodyUpdate = emailTemplate.update_basic_details_sms_content();
        notification.update_micro_campaign_notification(campaignId , 'micro', emailStatus, smsstatus, emailSender, emailBodyUpdate, emailSubjectUpdate, smsBodyUpdate);
    }
    if(newInfluencerData.length){
        let influencers = []
        newInfluencerData.forEach(function(results) {
            influencers.push(
                results.influencer_id
            );
        });
        notification.update_new_influencer_notification(influencers,campaignId, emailStatus, smsstatus, emailSender, emailBody, emailSubject, smsBody);
    }
}

/**
 * [Campaign notificaton for update premium section details]
*/
campaignPremiumDeatilsNotification = (campaignData, changesData,newInfluencerData) => {
    let campaignId = campaignData._id;
    let invitedstatus  = campaignData.is_invited;
    let microstatus  = campaignData.send_micro;
    let premiumstatus  = campaignData.send_premium;
    let emailStatus = campaignData.send_email;
    let smsstatus = campaignData.send_sms;
    let emailBody = campaignData.email_body;
    let emailSender = campaignData.email_sender;
    let emailSubject   = campaignData.email_subject;
    let smsBody  = campaignData.sms_body;
    if(changesData.length){
       for(let i=0; i < changesData.length; i++){
        const Influencer_id = Object.keys(changesData[i]);
        let [updateData] = Object.values(changesData[i]);

        let emailSubject = 'Campaign you accepted has been updated | Eleve';
        let emailBody = emailTemplate.update_basic_details_email_html(updateData);
        let smsBody = emailTemplate.update_basic_details_sms_content();
        notification.update_premium_campaign_notification(Influencer_id,campaignId, emailStatus, smsstatus, emailSender, emailBody, emailSubject, smsBody);
       }
    }
    if(newInfluencerData.length > 0){
        notification.update_new_influencer_notification(newInfluencerData,campaignId, emailStatus, smsstatus, emailSender, emailBody, emailSubject, smsBody);
    }
}

/* Export apis */
module.exports = {
    create_campaign_step_one,
    create_campaign_step_two_b,
    create_campaign_step_two_c,
    create_campaign_step_three
}