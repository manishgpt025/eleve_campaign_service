// Request Promise
const request = require('request-promise');
// Object ID
const ObjectId = require('mongodb').ObjectID;
// Helpers
const helper = require('../globalFunctions/function.js');
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const responseMessage = require('../globalFunctions/httpResponseMessage.js');
// Services
const adminService = require('../services/adminService.js');
const influencerService = require('../services/userServices.js');
const campaignService = require('../services/campaignServices.js');
const campaignInfluencersService = require('../services/campaignInfluencerServices.js');

/**
 * [Campaign Discard]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignDiscard = (req, res) => {
    let check = helper.checkRequest(["admin_id", "campaign_id"], req.body);
    if (check == true) {
        let query = {
            _id: new ObjectId(req.body.admin_id)
        };
        adminService.findData(query, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    // responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Admin found', resp);
                    // Check campaign status
                    let campaign_query = {
                        _id: new ObjectId(req.body.campaign_id)
                    };
                    campaignService.getDetails(campaign_query, {}, (campaign_error, campaign_resp) => {
                        if (campaign_error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, campaign_error);
                        } else {
                            // responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign Discarded', '');
                            if (campaign_resp) {
                                if (campaign_resp.status == '0' || campaign_resp.status == '5' || campaign_resp.status == '2') {
                                    // console.log("Status ==> ", campaign_resp.status);
                                    let campaign_query_1 = {
                                        _id: new ObjectId(req.body.campaign_id)
                                    };
                                    campaignService.deleteDetails(campaign_query_1, (campaign_error_1, campaign_resp_1) => {
                                        if (campaign_error_1) {
                                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, campaign_error_1);
                                        } else {
                                            let campaign_query_2 = {
                                                campaign_id: new ObjectId(req.body.campaign_id)
                                            };
                                            campaignInfluencersService.deleteDetails(campaign_query_2, (campaign_error_2, campaign_resp_2) => {
                                                if (campaign_error_2) {
                                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, campaign_error_2);
                                                } else {
                                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'The campaign has been discarded & trashed', campaign_resp_2);
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    responseHandle.sendResponseWithData(res, responseCode.BAD_REQUEST, 'Campaign could not discarded.');
                                }
                            } else {
                                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Campaign not found.');
                            }
                        }
                    });
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Campaign completed]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignCompleted = (req, res) => {
    let check = helper.checkRequest(["admin_id"], req.body);
    if (check == true) {
        let query = {
            _id: new ObjectId(req.body.admin_id)
        };
        adminService.findData(query, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    let campaign_query = {
                        "status": "6"
                    };
                    campaignService.getAllData(campaign_query, {}, (campaign_error, campaign_resp) => {
                        if (campaign_error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, campaign_error);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign List', campaign_resp);
                        }

                    });

                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }

            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Campaign delete]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignDelete = (req, res) => {
    let check = helper.checkRequest(["admin_id", "campaign_id"], req.body);
    if (check == true) {
        let query = {
            _id: new ObjectId(req.body.admin_id)
        };
        adminService.findData(query, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    let myquery = {
                        _id: new ObjectId(req.body.campaign_id)
                    };
                    let value = {
                        $set: {
                            status: "7",
                        }
                    }
                    campaignService.updateOne(myquery, value, { new: false }, (error, resp) => {
                        if (error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign deleted', '');
                        }

                    });

                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }

            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Campaign platform delete]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignPlatformDelete = async(req, res) => {
    let check = helper.checkRequest(["admin_id", "campaign_id", "platform_id", "platform", "type"], req.body);
    if (check == true) {
        let admin_email = req.body.decoded.email.toLowerCase();
        let query = {
            email: admin_email
        };
        adminService.findData(query, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    // Check campaign
                    let c_query = {
                        _id: new ObjectId(req.body.campaign_id)
                    };
                    campaignService.findData(c_query, async(c_error, c_resp) => {
                        if (c_error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, c_error);
                        } else {
                            if (c_resp) {
                                let resultData = await campaignPlatformDeleteFunction(c_resp.status, req.body.campaign_id,req.body.platform_id,req.body.platform,req.body.type );
                                if(resultData){
                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign platform deleted', '');
                                }else{
                                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Campaign platform not deleted');
                                }
                            } else {
                                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Campaign not found', '');
                            }
                        }
                    });
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}


/**
 * [Campaign Influencers]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignInfluencers = (req, res) => {
    let check = helper.checkRequest(["admin_id", "platform"], req.body);
    if (check == true) {
        let query = { _id: new ObjectId(req.body.admin_id)};
        adminService.findData(query, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    // Category
                    let category = ['A', 'B', 'Celeb', 'Celebrity'];
                    let platform = req.body.platform;
                    let platform_category = `${platform}.category`;
                    let bodyData = { [platform_category] : { $in: category }}
                    let options = {}
                    let influencers = [];
                    influencerService.premiumInfluncerSearch(bodyData, options, (campaign_error, campaign_resp) => {
                        if (campaign_error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, campaign_error);
                        } else {
                            if (campaign_resp) {
                                let result = campaign_resp;
                                for (let i = 0; i < result.length; i++) {
                                    let result_platform = result[i][platform];
                                    if(result_platform && result_platform.length > 0){
                                        let email = result[i].email;
                                        let inf_id = result[i]._id;
                                        result_platform.forEach(function(results) {
                                            // Check if handle exists
                                            if(category.indexOf(results.category) != -1 && results.Elv_social){
                                                influencers.push({
                                                    'cost': results.cost,
                                                    'category': results.category,
                                                    'followers_count': results.followers_count,
                                                    'Elv_social_id': results.id,
                                                    'profile_image_url': results.profile_image_url ? results.profile_image_url : "",
                                                    'Elv_social': results.Elv_social,
                                                    'influencer_email': email,
                                                    'influencer_id': inf_id
                                                });
                                            }
                                        });
                                    }
                                }
                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Premium Influencers List', influencers);
                            } else {
                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Premium Influencers List Not Found', []);
                            }
                        }
                    });
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Campaign edit status]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignEditStatus = (req, res) => {
    let check = helper.checkRequest(["admin_id", "campaign_id", "status"], req.body);
    if (check == true) {
        let query = {
            _id: new ObjectId(req.body.admin_id)
        };
        adminService.findData(query, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    let myquery = {
                        _id: new ObjectId(req.body.campaign_id)
                    };
                    let value = {
                        $set: {
                            status: req.body.status,
                        }
                    }
                    campaignService.updateOne(myquery, value, { new: true }, (error, resp) => {
                        if (error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign Status Update', resp);
                        }

                    });

                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }

            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [CSV Upload]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const uploadCSV = (req, res) => {
    let check = helper.checkRequest(["admin_id", "platform", "data"], req.body);
    if (check == true) {
        let platform = req.body.platform[0];
        let csv_data = req.body.data;
        if(csv_data.length < 1){
            responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Data not found.');
        }
        let queryData = { _id: new ObjectId(req.body.admin_id[0])}
        adminService.findData(queryData, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    let no_handle_found = 0;
                    let handle_found = 0;
                    let influencers = [];
                    let query = {};
                    let final_result = {};
                    query['registered'] = '1';
                    query['status'] = '1';
                    switch (platform) {
                        case 'twitter':
                            influencerService.getInfluencerData(query, { 'first_name': 1, 'email': 1, 'twitter': 1, 'last_name': 1 }, (err, result) => {
                                if (err) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                } else if (result.length > 0) {
                                    for (let i = 0; i < result.length; i++) {
                                        let result_twitter = result[i].twitter;
                                        if(result_twitter.length > 0){
                                            let email = result[i].email;
                                            let inf_id = result[i].id;
                                            csv_data.forEach(function(data) {
                                                result_twitter.forEach(function(results) {
                                                    // Check if handle exists
                                                    if (data.handle == results.Elv_social) {
                                                        if (results.category == 'C' || results.category == 'D') {
                                                            handle_found = handle_found + 1;
                                                            influencers.push({
                                                                'cost': results.cost,
                                                                'category': results.category,
                                                                'followers_count': results.followers_count,
                                                                'Elv_social_id': results.id,
                                                                'profile_image_url': results.profile_image_url,
                                                                'Elv_social': results.Elv_social,
                                                                'influencer_email': email,
                                                                'influencer_id': inf_id
                                                            });
                                                        }
                                                    }
                                                });
                                            });
                                        }
                                    }
                                    no_handle_found = csv_data.length - handle_found
                                    final_result = {
                                        "handle_found": handle_found,
                                        "no_handle_found": no_handle_found,
                                        "influencers": influencers
                                    }
                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer Found!', final_result);
                                    return;
                                } else {
                                    responseHandle.sendResponseWithData(res, 403, 'Influencer Not Found!');
                                }
                            });
                            break;
                            case 'instagram':
                                influencerService.getInfluencerData(query, { 'first_name': 1, 'email': 1, 'instagram': 1, 'last_name': 1 }, (err, result) => {
                                    if (err) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                    } else if (result.length > 0) {
                                        for (let i = 0; i < result.length; i++) {
                                            let result_instagram = result[i].instagram;
                                            if(result_instagram.length > 0){
                                                let email = result[i].email;
                                                let inf_id = result[i].id;
                                                csv_data.forEach(function(data) {
                                                    result_instagram.forEach(function(results) {
                                                        // Check if handle exists
                                                        if (data.handle == results.Elv_social) {
                                                            if (results.category == 'C' || results.category == 'D') {
                                                                handle_found = handle_found + 1;
                                                                influencers.push({
                                                                    'cost': results.cost,
                                                                    'category': results.category,
                                                                    'followers_count': results.followers_count,
                                                                    'Elv_social_id': results.id,
                                                                    'profile_image_url': results.profile_image_url,
                                                                    'Elv_social': results.Elv_social,
                                                                    'influencer_email': email,
                                                                    'influencer_id': inf_id
                                                                })
                                                            }
                                                        }
                                                    });
                                                });
                                            }
                                        }
                                        no_handle_found = csv_data.length - handle_found
                                        final_result = {
                                            "handle_found": handle_found,
                                            "no_handle_found": no_handle_found,
                                            "influencers": influencers
                                        }
                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer Found!', final_result);
                                        return;
                                    } else {
                                        responseHandle.sendResponseWithData(res, 403, 'Influencer Not Found!');
                                    }
                                });
                                break;
                                case 'facebook':
                                    influencerService.getInfluencerData(query, { 'first_name': 1, 'email': 1, 'facebook': 1, 'last_name': 1 }, (err, result) => {
                                        if (err) {
                                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                        } else if (result.length > 0) {
                                            for (let i = 0; i < result.length; i++) {
                                                let result_facebook = result[i].facebook;
                                                if(result_facebook.length > 0){
                                                    let email = result[i].email;
                                                    let inf_id = result[i].id;
                                                    csv_data.forEach(function(data) {
                                                        result_facebook.forEach(function(results) {
                                                            // Check if handle exists
                                                            if (data.handle == results.Elv_social) {
                                                                if (results.category == 'C' || results.category == 'D') {
                                                                    handle_found = handle_found + 1;
                                                                    influencers.push({
                                                                        'cost': results.cost,
                                                                        'category': results.category,
                                                                        'followers_count': results.followers_count,
                                                                        'Elv_social_id': results.id,
                                                                        'profile_image_url': results.profile_image_url,
                                                                        'Elv_social': results.Elv_social,
                                                                        'influencer_email': email,
                                                                        'influencer_id': inf_id
                                                                    })
                                                                }
                                                            }
                                                        });
                                                    });
                                                }
                                            }
                                            no_handle_found = csv_data.length - handle_found
                                            final_result = {
                                                "handle_found": handle_found,
                                                "no_handle_found": no_handle_found,
                                                "influencers": influencers
                                            }
                                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer Found!', final_result);
                                            return;
                                        } else {
                                            responseHandle.sendResponseWithData(res, 403, 'Influencer Not Found!');
                                        }
                                    });
                                    break;
                                    case 'youtube':
                                        influencerService.getInfluencerData(query, { 'first_name': 1, 'email': 1, 'youtube': 1, 'last_name': 1 }, (err, result) => {
                                            if (err) {
                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                            } else if (result.length > 0) {
                                                for (let i = 0; i < result.length; i++) {
                                                    let result_youtube = result[i].youtube;
                                                    if(result_youtube.length > 0){
                                                        let email = result[i].email;
                                                        let inf_id = result[i].id;
                                                        csv_data.forEach(function(data) {
                                                            result_youtube.forEach(function(results) {
                                                                // Check if handle exists
                                                                if (data.handle == results.Elv_social) {
                                                                    if (results.category == 'C' || results.category == 'D') {
                                                                        handle_found = handle_found + 1;
                                                                        influencers.push({
                                                                            'cost': results.cost,
                                                                            'category': results.category,
                                                                            'followers_count': results.followers_count,
                                                                            'Elv_social_id': results.id,
                                                                            'profile_image_url': results.profile_image_url,
                                                                            'Elv_social': results.Elv_social,
                                                                            'influencer_email': email,
                                                                            'influencer_id': inf_id
                                                                        })
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    }
                                                }
                                                no_handle_found = csv_data.length - handle_found
                                                final_result = {
                                                    "handle_found": handle_found,
                                                    "no_handle_found": no_handle_found,
                                                    "influencers": influencers
                                                }
                                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer Found!', final_result);
                                                return;
                                            } else {
                                                responseHandle.sendResponseWithData(res, 403, 'Influencer Not Found!');
                                            }
                                        });
                                        break;
                                        default:
                                                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Platform Not Found!');
                    }
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [CSV Upload Premium]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const premiumUploadCSV = (req, res) => {
    let check = helper.checkRequest(["admin_id", "platform", "data"], req.body);
    if (check == true) {
        let platform = req.body.platform[0];
        let csv_data = req.body.data;
        let queryData = { _id: new ObjectId(req.body.admin_id[0])};
        let no_handle_found = 0;
        let handle_found = 0;
        let influencers = [];
        let query = {};
        let final_result = {};
        query['registered'] = '1';
        query['status'] = '1';
        adminService.findData(queryData, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    switch (platform) {
                        case 'twitter':
                            influencerService.getInfluencerData(query, { 'first_name': 1, 'email': 1, 'twitter': 1, 'last_name': 1 }, (err, result) => {
                                if (err) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                } else if (result.length > 0) {
                                    for (let i = 0; i < result.length; i++) {
                                        let result_twitter = result[i].twitter;
                                        if(result_twitter.length > 0){
                                            let email = result[i].email;
                                            let inf_id = result[i].id;
                                            csv_data.forEach(function(data) {
                                                result_twitter.forEach(function(results) {
                                                    // Check if handle exists
                                                    if (data.handle == results.Elv_social) {
                                                        if (results.category == 'A' || results.category == 'B' || results.category == 'Celebrity' || results.category == 'Celeb') {
                                                            handle_found = handle_found + 1;
                                                            influencers.push({
                                                                'cost': results.cost,
                                                                'category': results.category,
                                                                'followers_count': results.followers_count,
                                                                'Elv_social_id': results.id,
                                                                'profile_image_url': results.profile_image_url ? results.profile_image_url : "",
                                                                'Elv_social': results.Elv_social,
                                                                'influencer_email': email,
                                                                'influencer_id': inf_id
                                                            })
                                                        }
                                                    }
                                                });
                                            });
                                        }
                                    }
                                    no_handle_found = csv_data.length - handle_found
                                    final_result = {
                                        "handle_found": handle_found,
                                        "no_handle_found": no_handle_found,
                                        "influencers": influencers
                                    }
                                    responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer Found!', final_result);
                                    return;
                                } else {
                                    responseHandle.sendResponseWithData(res, 403, 'Influencer Not Found!');
                                }
                            });
                            break;
                            case 'instagram':
                                influencerService.getInfluencerData(query, { 'first_name': 1, 'email': 1, 'instagram': 1, 'last_name': 1 }, (err, result) => {
                                    if (err) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                    } else if (result.length > 0) {
                                        for (let i = 0; i < result.length; i++) {
                                            let result_instagram = result[i].instagram;
                                            if(result_instagram.length > 0){
                                                let email = result[i].email;
                                                let inf_id = result[i].id;
                                                csv_data.forEach(function(data) {
                                                    result_instagram.forEach(function(results) {
                                                        // Check if handle exists
                                                        if (data.handle == results.Elv_social) {
                                                            if (results.category == 'A' || results.category == 'B' || results.category == 'Celebrity' || results.category == 'Celeb') {
                                                                handle_found = handle_found + 1;
                                                                influencers.push({
                                                                    'cost': results.cost,
                                                                    'category': results.category,
                                                                    'followers_count': results.followers_count,
                                                                    'Elv_social_id': results.id,
                                                                    'profile_image_url': results.profile_image_url ? results.profile_image_url : "",
                                                                    'Elv_social': results.Elv_social,
                                                                    'influencer_email': email,
                                                                    'influencer_id': inf_id
                                                                });
                                                            }
                                                        }
                                                    });
                                                });
                                            }
                                        }
                                        no_handle_found = csv_data.length - handle_found
                                        final_result = {
                                            "handle_found": handle_found,
                                            "no_handle_found": no_handle_found,
                                            "influencers": influencers
                                        }
                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer Found!', final_result);
                                        return;
                                    } else {
                                        responseHandle.sendResponseWithData(res, 403, 'Influencer Not Found!');
                                    }
                                });
                                break;
                                case 'facebook':
                                    influencerService.getInfluencerData(query, { 'first_name': 1, 'email': 1, 'facebook': 1, 'last_name': 1 }, (err, result) => {
                                        if (err) {
                                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                        } else if (result.length > 0) {
                                            for (let i = 0; i < result.length; i++) {
                                                let result_facebook = result[i].facebook;
                                                if(result_facebook.length > 0){
                                                    let email = result[i].email;
                                                    let inf_id = result[i].id;
                                                    csv_data.forEach(function(data) {
                                                        result_facebook.forEach(function(results) {
                                                            // Check if handle exists
                                                            if (data.handle == results.Elv_social) {
                                                                if (results.category == 'A' || results.category == 'B' || results.category == 'Celebrity' || results.category == 'Celeb') {
                                                                    handle_found = handle_found + 1;
                                                                    influencers.push({
                                                                        'cost': results.cost,
                                                                        'category': results.category,
                                                                        'followers_count': results.followers_count,
                                                                        'Elv_social_id': results.id,
                                                                        'profile_image_url': results.profile_image_url ? results.profile_image_url : "",
                                                                        'Elv_social': results.Elv_social,
                                                                        'influencer_email': email,
                                                                        'influencer_id': inf_id
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    });
                                                }
                                            }
                                            no_handle_found = csv_data.length - handle_found
                                            final_result = {
                                                "handle_found": handle_found,
                                                "no_handle_found": no_handle_found,
                                                "influencers": influencers
                                            }
                                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer Found!', final_result);
                                            return;
                                        } else {
                                            responseHandle.sendResponseWithData(res, 403, 'Influencer Not Found!');
                                        }
                                    });
                                    break;
                                    case 'youtube':
                                        influencerService.getInfluencerData(query, { 'first_name': 1, 'email': 1, 'youtube': 1, 'last_name': 1 }, (err, result) => {
                                            if (err) {
                                                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, err);
                                            } else if (result.length > 0) {
                                                for (let i = 0; i < result.length; i++) {
                                                    let result_youtube = result[i].youtube;
                                                    if(result_youtube.length > 0){
                                                        let email = result[i].email;
                                                        let inf_id = result[i].id;
                                                        csv_data.forEach(function(data) {
                                                            result_youtube.forEach(function(results) {
                                                                // Check if handle exists
                                                                if (data.handle == results.Elv_social) {
                                                                    if (results.category == 'A' || results.category == 'B' || results.category == 'Celebrity'|| results.category == 'Celeb') {
                                                                        handle_found = handle_found + 1;
                                                                        influencers.push({
                                                                            'cost': results.cost,
                                                                            'category': results.category,
                                                                            'followers_count': results.followers_count,
                                                                            'Elv_social_id': results.id,
                                                                            'profile_image_url': results.profile_image_url ? results.profile_image_url : "",
                                                                            'Elv_social': results.Elv_social,
                                                                            'influencer_email': email,
                                                                            'influencer_id': inf_id
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    }
                                                }
                                                no_handle_found = csv_data.length - handle_found
                                                final_result = {
                                                    "handle_found": handle_found,
                                                    "no_handle_found": no_handle_found,
                                                    "influencers": influencers
                                                }
                                                responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer Found!', final_result);
                                                return;
                                            } else {
                                                responseHandle.sendResponseWithData(res, 403, 'Influencer Not Found!');
                                            }
                                        });
                                        break;
                                        default:
                                            responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Platform Not Found.');
                    }
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Campaign platform delete]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignImageDelete = (req, res) => {
    let check = helper.checkRequest(["admin_id", "campaign_id", "image_id"], req.body);
    if (check == true) {
        let query = {
            _id: new ObjectId(req.body.admin_id)
        };
        adminService.findData(query, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    // Check campaign
                    let c_query = {
                        _id: new ObjectId(req.body.campaign_id)
                    };
                    campaignService.findData(c_query, (c_error, c_resp) => {
                        if (c_error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, c_error);
                        } else {
                            if (c_resp) {
                                let myquery = {
                                    _id: new ObjectId(req.body.campaign_id),
                                    "images._id": new ObjectId(req.body.image_id)
                                };
                                let value = {
                                    $pull: {
                                        "images": { "_id": new ObjectId(req.body.image_id) }
                                    }
                                }
                                campaignService.updateOne(myquery, value, { new: true }, (error, resp) => {
                                    if (error) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                    } else {
                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Image removed!', '');
                                    }
                                });
                            } else {
                                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Campaign not found', '');
                            }
                        }
                    });
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Campaign influencer remove]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignInfluencerRemove = (req, res) => {
    let check = helper.checkRequest(["admin_id", "campaign_id", "influencers"], req.body);
    if (check == true) {
        let query = {
            _id: new ObjectId(req.body.admin_id)
        };
        adminService.findData(query, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    // Check campaign
                    let c_query = {
                        _id: new ObjectId(req.body.campaign_id)
                    };
                    campaignService.findData(c_query, (c_error, c_resp) => {
                        if (c_error) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, c_error);
                        } else {
                            if (c_resp) {
                                let myquery = {
                                    "campaign_id": new ObjectId(req.body.campaign_id),
                                    "influencer_id": { $in: req.body.influencers }
                                };

                                campaignInfluencersService.removeRecord(myquery, {}, (error, resp) => {
                                    if (error) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                    } else {
                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer removed from campaign!', '');
                                    }
                                });

                            } else {
                                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Campaign not found', '');
                            }
                        }
                    });
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Campaign influencer platform delete]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignInfluencerHandleRemove = (req, res) => {
    let check = helper.checkRequest(["admin_id", "handle_id"], req.body);
    if (check == true) {
        let query = {
            _id: new ObjectId(req.body.admin_id)
        };
        adminService.findData(query, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    let myquery = {
                        _id: new ObjectId(req.body.handle_id)
                    };
                    campaignInfluencersService.removeRecord(myquery, {}, (error_, resp_) => {
                        if (error_) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error_);
                        } else {
                            responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Influencer handle removed from campaign!', '');
                        }
                    });
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }
            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Campaign stop]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignStopStatus = (req, res) => {
    let check = helper.checkRequest(["admin_id", "campaign_id", "status", "end_date"], req.body);
    if (check == true) {
        let query = {
            _id: new ObjectId(req.body.admin_id)
        };
        adminService.findData(query, (error, resp) => {
            if (error) {
                responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
            } else {
                if (resp) {
                    let myquery = {
                        _id: new ObjectId(req.body.campaign_id)
                    };
                    campaignService.findData(myquery, (err_, result_) => {
                        if (err_) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);

                        } else {
                            if (result_.status == req.body.status) {

                                let value = {
                                    $set: {
                                        status: '4',
                                        end_date: req.body.end_date
                                    }
                                }
                                campaignService.updateOne(myquery, value, { new: true }, (error, resp) => {
                                    if (error) {
                                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                                    } else {
                                        responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Campaign Status Update', resp);
                                    }

                                });
                            } else {
                                responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Campaign not found. ');

                            }
                        }
                    });
                } else {
                    responseHandle.sendResponseWithData(res, responseCode.NOT_FOUND, 'Admin not found. ');
                }

            }
        });
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
}

/**
 * [Campaign platform delete function]
*/
const campaignPlatformDeleteFunction = async (campaign_status, campaign_id,platform_id="",platform,type ) => {
    if(platform_id){
        let myquery = {
            _id: new ObjectId(campaign_id),
            "platforms._id": new ObjectId(platform_id)
        };
        let value = {
            $pull: {
                "platforms": { "_id": new ObjectId(platform_id) }
            }
        }
        await updateCamapignPlatformData(myquery,value,{new:true});
    }
    myquery = {
        _id: new ObjectId(campaign_id),
        "number_influencer.name": platform,
        "number_influencer.type": type

    };
    value = {
        $pull: {
            "number_influencer": {
                "name": platform,
                "type": type
            }
        }
    }
    await updateCamapignPlatformData(myquery,value,{new:false});
    myquery = {
        _id: new ObjectId(campaign_id),
        "number_post.name": platform,
        "number_post.type": type

    };
    value = {
        $pull: {
            "number_post": {
                "name": platform,
                "type": type
            }
        }
    }
    await updateCamapignPlatformData(myquery,value,{new:false});
    myquery = {
        _id: new ObjectId(campaign_id),
        "conversations.name": platform,
        "conversations.type": type

    };
    value = {
        $pull: {
            "conversations": {
                "name": platform,
                "type": type
            }
        }
    }
    await updateCamapignPlatformData(myquery,value,{new:false});
    myquery = {
        _id: new ObjectId(campaign_id),
        "mentions.platform": platform,

    };
    value = {
        $pull: {
            "mentions": {
                "platform": platform,
            }
        }
    }
    await updateCamapignPlatformData(myquery,value,{new:false});
    myquery = {
        _id: new ObjectId(campaign_id),
        "platform_specific.platform": platform,

    };
    value = {
        $pull: {
            "platform_specific": {
                "platform": platform,
            }
        }
    }
    await updateCamapignPlatformData(myquery,value,{new:false});
    myquery = {
        _id: new ObjectId(campaign_id),
        "description.platform": platform,

    };
    value = {
        $pull: {
            "description": {
                "platform": platform,
            }
        }
    }
    await updateCamapignPlatformData(myquery,value,{new:false});
    myquery = {
        _id: new ObjectId(campaign_id),
        "sample.platform": platform,

    };
    value = {
        $pull: {
            "sample": {
                "platform": platform,
            }
        }
    }
    await updateCamapignPlatformData(myquery,value,{new:false});
    let reqData = {
        "campaign_id": new ObjectId(campaign_id),
        "platform": platform,
    };
    await campaignService.deletedescriptionDetail(reqData);

    // Delete from campaign_influencers
    if(campaign_status != '3'){
        let campaign_query_1 = {
            "campaign_id": campaign_id,
            "platform": platform,
            "type": type
        };
        await campaignInfluencersService.deleteDetailsAsync(campaign_query_1);
    }
    return true;
}

updateCamapignPlatformData = async (query, bodydata, options) => {
    let result = '';
    result = await campaignService.updateOneAsync(query,bodydata, options);
        if (result) {
            return result;
        }
        return result;
}


/*Export apis*/
module.exports = {
    campaignDiscard,
    campaignCompleted,
    campaignDelete,
    campaignPlatformDelete,
    campaignInfluencers,
    campaignEditStatus,
    uploadCSV,
    premiumUploadCSV,
    campaignImageDelete,
    campaignInfluencerRemove,
    campaignInfluencerHandleRemove,
    campaignStopStatus,
    campaignPlatformDeleteFunction
}