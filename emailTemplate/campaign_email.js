const update_basic_details_email_html = (updateData) =>{
    let html =``;
    html = `<p>Hello &lt;influencer_name&gt;<influencer_name>,</p>
    <p>There have been changes in the recent LIVE campaign you accepted. Follow &lt;link&gt; <link> to visit details page.</p>
    <p>Items updated are – </p>`;

    for (i = 0; i < updateData.length; i++) {
        html+=`<p>${updateData[i]}</p>`;
    }
    html += `<p>Please login to your account at user.eleveglobal.com to access campaigns</p>

    <p>Happy to have you!</p>
    <p>Team Eleve Media</p>

    <p>Brand New Panel - user.eleveglobal.com</p>
    <p>India Website &ndash; eleve.co </p>
    <p>Global Website - eleveglobal.com</p>`;

    return html;
}

const update_basic_details_sms_content = () =>{
    return 'ELEVE LIVE CAMPAIGN UPDATED A live campaign you accepted has been updated. Follow <link> to view changes in detail & participate.Team Eleve';
}

module.exports = {
    update_basic_details_email_html,
    update_basic_details_sms_content
}