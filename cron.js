const campaignCron = require('./cronServices/campaign_cron');
const cron = require('node-cron');
//Call Campaign related Crons

campaign_crons = () =>{
    let intervalTime = "* * * * *";
    cron.schedule(intervalTime, () => {
        campaignCron.scheduleToLiveCampaign();
        campaignCron.liveToEndCampaign();
        campaignCron.endToCompleteCampaign();
    });
}

//call function
campaign_crons();