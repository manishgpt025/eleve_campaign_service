#!/bin/sh
  
image="registry.eleveglobal.com/campaign_service"  
  
#get timestamp for the tag  
timestamp=$(date +"v%d.%m.%y")  

#get hash tag of git
hashtag=$(git log -1 --pretty=%h)
  
tag=$image:$hashtag.$timestamp

#build image  
sudo docker build -t $tag .  

#push to dockerhub  
docker login -u eleve -p eleve@321 registry.eleveglobal.com
#sudo docker login -u username -p password  
sudo docker push $image  
  
#remove dangling images  
sudo docker system prune -f  
