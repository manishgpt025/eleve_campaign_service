module.exports = {
  development: {
    node_port: process.env.IDEATION_PORT,
    database : process.env.DATABASE_NAME,
    username : process.env.username,
    password : process.env.PASSWORD,
    db_host : process.env.DB_HOST,
    url: process.env.SERVICE_HOST
  },
  stagging: {
    node_port: process.env.DEV_IDEATION_PORT,
    database : process.env.DATABASE_NAME,
    username : process.env.USERNAME,
    password : process.env.PASSWORD,
    db_host : process.env.DB_HOST,
    url: process.env.DEV_SERVICE_HOST
  }
};
