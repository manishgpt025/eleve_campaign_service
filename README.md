# Eleve Global - Campaign Services

Node version 10.X.

## Development server

Clone from repository `git clone https://{bitbucket_user_name}@bitbucket.org/elevetech/campaign-services.git`

Run `npm i` for package dependencies.

Run `nodemon` for a dev server. 

This service will be run on port `3002` and you can check this URL `http://localhost:3002/`.

## Note

After this you need to run the other micro services as `user_services, ideation_services and campaign_services` to communicate with the system.

