const router = require('express').Router();
const campaignCreateController = require("../controllers/campaign-create.js");
const campaignController_ = require("../controllers/campaignController");
//const campaignListController = require("../controllers/campaign-list");
const campaignController = require("../controllers/campaign");
const manageKolController = require("../controllers/manageKolController");


/* Routes */
router.post('/create_campaign_step_one', campaignCreateController.create_campaign_step_one);
router.post('/create_campaign_step_two_b', campaignCreateController.create_campaign_step_two_b);
router.post('/create_campaign_step_two_c', campaignCreateController.create_campaign_step_two_c);
router.post('/create_campaign_step_three', campaignCreateController.create_campaign_step_three);

router.post('/discard', campaignController_.campaignDiscard);
router.post('/completed', campaignController_.campaignCompleted);
router.post('/delete', campaignController_.campaignDelete);
router.post('/platform/delete', campaignController_.campaignPlatformDelete);
router.post('/influencers', campaignController_.campaignInfluencers);
router.post('/edit/status', campaignController_.campaignEditStatus);
router.post('/upload/csv', campaignController_.uploadCSV);
router.post('/premium/csv/upload', campaignController_.premiumUploadCSV);
router.post('/image/delete', campaignController_.campaignImageDelete);
router.post('/influencer/remove', campaignController_.campaignInfluencerRemove);
router.post('/influencer/handle/remove', campaignController_.campaignInfluencerHandleRemove);

/* Routes */
//router.get('/list', campaignListController.campaignList);
/*campaign manager list */
router.post('/managerlist', campaignController.managerList);
// search agency or brand
router.post('/searchagency', campaignController.searchAgency);
// search advertiser
router.post('/searchadvertiser', campaignController.searchAdvertiser);
// search brands related to agency advertiser
router.post('/searchbrand', campaignController.searchBrand);
// get details of campaign by id
router.post('/campaigndetails', campaignController.campaignDetailsById);
//filters for influencers
router.post('/searchinfluencers', campaignController.searchInfluencer);
//campaign list
router.post('/campaignlist', campaignController.campaignList);
//campaignstop
router.post('/campaignstopstatus', campaignController_.campaignStopStatus);








//********************************kol platform list***************************//
router.post('/kol/campaignplatform', manageKolController.campaignPlatformListing);
//kol listing search
router.post('/kol/microSearch', manageKolController.microSearch);
// add micro influencer in kol
router.post('/kol/addmicroinfluencer', manageKolController.addMicroInfluencer);
//Campaign manage kol list
router.post('/kol/campaignKolsList', manageKolController.campaignKolsList);
// Export API
module.exports = router;