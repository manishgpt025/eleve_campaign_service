const cron = require('node-cron');
// Date and time
let date = require('date-and-time');
const campaignModel = require('../models/campaign.js');
const notification = require('../globalFunctions/mail_sms_function.js');
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('CG0lEmHWZK4req6olYnGOQ');
var AWS = require('aws-sdk');

/**
 * [create campaign live cron from schedule and start date]
 */
const scheduleToLiveCampaign = async () => {
    let now = new Date();
    let convertData = date.format(now, 'MM/DD/YYYY HH:mm:ss');
    let query = {
        "start_date": {
            "$lte": convertData
        },
        'status': '2'
    };
    let update = {
        "$set": {
            "status": "3"
        }
    }
    try {
        let allScheduleCampaign = await campaignModel.find(
            query,
            {
            'start_date' : 1,
            'end_date' : 1 ,
            'status': 1,
            'is_invited' : 1,
            'send_micro' : 1,
            'send_premium' : 1,
            'send_email' : 1,
            'send_sms' : 1,
            'email_body' : 1,
            'email_sender' : 1,
            'email_subject' : 1,
            'sms_body' : 1
        }).lean();
        allScheduleCampaign.map(async (camp, index) => {
        	let campaign = { camp} ;
        	let campaignId = campaign['camp']._id;
        	let invitedstatus  = campaign['camp'].is_invited;
        	let microstatus  = campaign['camp'].send_micro;
        	let premiumstatus  = campaign['camp'].send_premium;
        	let emailStatus = campaign['camp'].send_email;
        	let smsstatus = campaign['camp'].send_sms;
        	let emailBody = campaign['camp'].email_body;
        	let emailSender = campaign['camp'].email_sender;
        	let emailSubject   = campaign['camp'].email_subject;
        	let smsBody  = campaign['camp'].sms_body;
            await campaignModel.update(
                {'_id' : campaignId},
                update, (error_, res) => {
                    console.log("campaign update for live.");
                    if (error_) {
                        console.log(error_);
                    } else {
                        if(invitedstatus == true) {
                            if(microstatus === '1') {
                                console.log("send for micro.",microstatus);
                                notification.live_campaign_notification(campaignId , 'micro', emailStatus, smsstatus, emailSender, emailBody, emailSubject, smsBody);
                            }
                            // if(premiumstatus == '1') {
                            // 	addInfluencers(campaignId , 'premium', emailStatus, smsstatus, emailSender, emailBody, emailSubject, smsBody);
                            // }
                        }
                    }
            });
	    });
	} catch (e) {
		throw new Error(e)
	}
}

// =============================================================================================================================================//

/**
 * [create campaign end cron from Live and end date]
 */
const liveToEndCampaign = async () => {
    let now = new Date();
    let convertData = date.format(now, 'MM/DD/YYYY HH:mm:ss');
    let query = {
        "end_date": {
            "$lte": convertData
        },
        'status': '3'
    };
    let update = {
        "$set": {
            "status": "4"
        }
    }
    let allLiveUpdateCampaign = await campaignModel.updateMany(query, update);
    console.log("campaign End updated.", allLiveUpdateCampaign);
}

/**
 * [create campaign complete cron from End and end date]
 */
const endToCompleteCampaign = async () => {
    let query = {
        'status': '4'
    };
    let update = {
        "$set": {
            "status": "6"
        }
    }
    const result = await campaignModel.find(query,
     {
        'start_date' : 1,
        'end_date' : 1 ,
        'status': 1
    });
    if(result.length){
        result.map((camp, index) => {
            let campaignId = camp._id;
            let camp_end_date = camp.end_date;
            let camp_status = camp.status;

            let current_date = new Date();
            let future_end = new Date( (camp_end_date) );
            let end_date = new Date(future_end.getTime()+1000*60*60*24);
            let convertedEndDate = date.format(end_date, 'MM/DD/YYYY HH:mm:ss');
            let convertedCurrentDate = date.format(current_date, 'MM/DD/YYYY HH:mm:ss');
            if(
                convertedEndDate <= convertedCurrentDate
                    &&
                camp_status == '4'
            ){
                campaignModel.update(
                    {'_id' : campaignId},
                    update,
                    (error_, allLiveUpdateCampaign) => {
                        if (error_) {
                            console.log(error_);
                        } else {
                            console.log("campaign Complete updated.", allLiveUpdateCampaign);
                        }
                    }
                );
            }
        });
    }else{
        console.log("campaign Complete not found.");
    }
}

module.exports = {
    scheduleToLiveCampaign,
    liveToEndCampaign,
    endToCompleteCampaign
}