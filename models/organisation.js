const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;
let organisationModel = new schema({
    organisation_id: {
        type: String //autogenerrated unique id
    },
    organisation_name: {
        type: String // name of organisation
    },
    type: {
        type: String //type of organisation
    },
    social_media: {
        type: Array //stores media like facebook, instagram, twitter
    },
    associated_brands: [{
        brands: {
            type: Array // brands with key and name
        },
        advertiser_id: {
            type: String //advertiser id
        }
    }],
    is_asscoiated: {
        type: String, //is_associated
        default: "0"
    },
    addresses: [{
        advertisers: {
            type: Array //ids of sdvertiser
        },
        city: {
            type: String //city of advertiser
        },
        state: {
            type: String //state of advertiser
        },
        country: {
            type: String //country of advertiser
        },
        address: {
            type: String //address of advertiser
        }
    }],

});

organisationModel.plugin(mongoosePaginate)
organisationModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('organisation', organisationModel);