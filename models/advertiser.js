const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;
let advertiserModel = new schema({
    advertiser_id: {
        type: String //autogenerrated unique id
    },
    name: {
        type: String //name of advertiser
    },
    last_name: {
        type: String //last_name of advertiser
    },
    organisation: {
        type: String //type
    },
    email: {
        type: String //email of advertiser
    },
    phone: {
        type: String //phone of advertiser
    },
    country: {
        type: String //country of advertiser
    },
    state: {
        type: String //state of advertiser
    },
    city: {
        type: String //city of advertiser
    },
    address: {
        type: String //address of advertiser
    },
    status: {
        type: String //status(0 or 1) of advertiser
    },
    password: {
        type: String //password of advertiser
    },
    designation: {
        type: String //country of advertiser
    },
    isd: {
        type: String //designation of advertiser
    },
    manager: {
        type: String //manager of advertiser
    },
    notes: {
        type: String //notes of advertiser
    },
    organisation_id: {
        type: String //organisation_id of advertiser
    },
    associated_brands: {
        type: Array
    },
    profile_image: {
        type: String //image of advertiser
    },
    created_at: {
        type: Date, //created date
        default: Date.now
    },
    updated_at: {
        type: Date //updated at
    }


});

// module.exports = mongoose.model('advertiser', advertiserModel);
advertiserModel.plugin(mongoosePaginate)
advertiserModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('advertiser', advertiserModel);