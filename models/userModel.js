const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let InfluencerModel = new schema({
    influencer_id: {
        type: String //autogenerrated unique id
    },
    first_name: {
        type: String
    },
    last_name: {
        type: String
    },
    email: {
        type: String
    },
    alt_email: {
        type: String
    },
    mobile: {
        type: String
    },
    alt_mobile: {
        type: String
    },
    password: {
        type: String
    },
    resetPasswordToken: {
        type: String
    },
    resetPasswordExpires: {
        type: Date
    },
    campaign_notification: {
        type: Array
    },
    payment_notification: {
        type: Array
    },
    account_notification: {
        type: Array
    },
    new_feature_notification: {
        type: Array
    },
    email_verification: {
        type: String
    },
    emailverificationToken: {
        type: String
    },
    emailverificationExpires: {
        type: String
    },
    interests: {
        type: Array
    },
    language: {
        type: Array
    },
    lifestage: {
        type: Array
    },
    education: {
        type: Array
    },
    income: {
        type: Array
    },
    bank_details: {
        type: Array
    },
    payment_details: {
        type: Array
    },
    twitter: {
        type: Array
    },
    facebook: {
        type: Array
    },
    youtube: {
        type: Array
    },
    linkedin: {
        type: Array
    },
    instagram: {
        type: Array
    },
    pinterest: {
        type: Array
    },
    blog: {
        type: Array
    },
    instagram_business: {
        type: Array
    },
    tiktok: {
        type: Array
    },
    address: {
        type: String
    },
    dob: {
        type: String
    },
    country: {
        type: String
    },
    state: {
        type: String
    },
    city: {
        type: String
    },
    isd_code: {
        type: String
    },
    alt_isd_code: {
        type: String
    },
    isd_mobile: {
        type: String
    },
    isd_alt_mobile: {
        type: String
    },
    gender: {
        type: String
    },
    otp: {
        type: String
    },
    is_verified: {
        type: String
    },
    profile_pic: {
        type: String
    },
    registered: {
        type: String
    },
    note: {
        type: String
    },
    status: {
        type: String,
        default: "",
        enum: ['', '1', '0']
    }


});

InfluencerModel.plugin(mongoosePaginate)
InfluencerModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('influencers', InfluencerModel);