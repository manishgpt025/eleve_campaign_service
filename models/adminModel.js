const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;
let adminModel = new schema({
    admin_id: {
        type: String
    },

    date: {
        type: String
    },
    email: {
        type: String
    },
    last_login_at: {
        type: String
    },
    logins: {
        type: Array
    },
    name: {
        type: String
    },
    password: {
        type: String
    },
    permission: {
        type: String
    },
    status: {
        type: String
    },
    address: {
        type: String
    },
    status: {
        type: String
    },
    password: {
        type: String
    },
    permission: {
        type: String
    },
    login: {
        type: String
    },
    last_login_from: {
        type: String
    },
    role: {
        type: String
    },
    mobile: {
        type: String
    },
    isd: {
        type: String
    },
    team: {
        type: String
    },
    modules: {
        type: Array
    },
    dob: {
        type: String
    },
    location: {
        type: String
    },
    extension: {
        type: String
    },
    first_login: {
        type: String
    },
    last_name: {
        type: String
    },
    profile_image: {
        type: String
    }

});
adminModel.plugin(mongoosePaginate)
adminModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('admin_user', adminModel);