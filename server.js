const dotenv = require('dotenv');
dotenv.config();
const express = require('express')
const app = express();
require('dotenv').config();
const config = require('./config.js');
bodyParser = require('body-parser');
//For DB
const dbConnect = require('./mongodb');
//For Cron
require('./cron');
//For Route
const route = require('./routes/campaignRoute.js');

const server = require('http').createServer(app)
cors = require('cors');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

//call route by api
app.use('/campaign', route);

//connectivity forn listen PORT
server.listen(process.env.CAMPAIGN_PORT, () => {
    console.log('Campaign listening on port', process.env.CAMPAIGN_PORT)
});