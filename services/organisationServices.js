const organisationModel = require('../models/organisation.js');

// Add data to organisationModel
const createData = (bodyData, callback) => {
    organisationModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}

// get data / find by element - returns only one record
const getData = (bodyData, callback) => {
    organisationModel.findOne(bodyData, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

// find data / find by element - returns only one record
const findData = (bodyData, callback) => {
    organisationModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

const findDataBy = (bodyData, callback) => {
    organisationModel.findOne(bodyData, { "addresses.$": 1 }, (err, result) => {
        callback(err, result);
    });
}

// update only one record
const updateOne = (query, bodyData, options, callback) => {
    organisationModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

// update only one record
const getDetails = (bodydata, options, callback) => {
    organisationModel.findOne(bodydata, options, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

//
const getDetails_pagination = (bodydata, options, page, callback) => {
    organisationModel.findOne(bodydata, options, page, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

//
const updateDetails = (query, bodydata, options, callback) => {
    organisationModel.findByIdAndUpdate(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

//
const deleteDetails = (query, bodydata, options, callback) => {
    organisationModel.findByIdAndRemove(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

// find by document id and update and push item in array
const arrayUpdateRemove = (query, data, options, callback) => {
    organisationModel.findByIdAndUpdate(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

// find by document id and array key and set item in array
const arrayUpdate = (query, data, options, callback) => {
    organisationModel.updateOne(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

//
const getOnlyData = (data, options, callback) => {
    organisationModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}

// Get all organisations
const getAllData = (data, options, callback) => {
    organisationModel.find(data, options, (err, result) => {
        callback(err, result);
    });
}

// Get record by pagination
const getPaginateData = (data, options, callback) => {
    organisationModel.paginate(data, options, (err, result) => {
        callback(err, result);
    });
}

// Get all data with pagination using aggregate
const getPaginateDataWithAggregate = (bodyData, options, sort, callback) => {
    console.log(bodyData)
    var aggregate = organisationModel.aggregate([{
            $match: {
                $or: [{
                    organisation_name: {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }]
            },
        },
        {
            $match: { type: { $in: bodyData.status } },
        },
        {
            "$project": {
                "_id": { "$toString": "$_id" },
                "associated_brands": 1,
                "organisation_name": 1,
                "type": 1
            }
        },
        {
            $lookup: {
                from: "advertisers",
                localField: "_id",
                foreignField: "organisation_id",
                as: "count_data"
            }
        },
        {
            $unwind:
            //  "$associated_brands"
            {
                "path": "$associated_brands",
                "preserveNullAndEmptyArrays": true
            }

        },
        {
            $lookup: {
                from: "organisations",
                "let": { "associated_brands": "$associated_brands" },
                "pipeline": [{ "$match": { "$expr": { "$in": [{ "$toString": "$_id" }, "$associated_brands.brands"] } } }],
                //  {"$match":{"associated_brands":{"$ne":[]}}},
                //{ "fieldToCheck": { $exists: true, $ne: null } },
                // pipeline: [{
                //     $match: {
                //         $expr: {
                //             $and: [
                //                 {"$associated_brands": { $ne: [ "$$associated_brands.brands", [] ] }},
                //                 {
                //                   $in: [{ "$toString": "$_id" }, "$$associated_brands.brands"]
                //                 }
                //             ]
                //         }
                //     }
                // }],
                as: "associated_brands_info"
            }
        },
        {
            $project: {
                _id: 1,
                "associated_brands": 1,
                "organisation_name": 1,
                "type": 1,
                "advertiserCount": { $size: "$count_data" },
                "associated_brands_info": 1
            }
        }, { $sort: sort }

    ])
    organisationModel.aggregatePaginate(aggregate, options, (error, success, pages, total) => {
        callback(error, success, pages, total);
    })
}

module.exports = {
    "createData": createData,
    "getData": getData,
    "updateOne": updateOne,
    "getDetails": getDetails,
    "updateDetails": updateDetails,
    "deleteDetails": deleteDetails,
    "getDetails_pagination": getDetails_pagination,
    "findData": findData,
    "findDataBy": findDataBy,
    "arrayUpdateRemove": arrayUpdateRemove,
    "getOnlyData": getOnlyData,
    "arrayUpdate": arrayUpdate,
    "getAllData": getAllData,
    "getPaginateData": getPaginateData,
    "getPaginateDataWithAggregate": getPaginateDataWithAggregate
}