const campaignInfluencerModel = require('../models/campaign_influencer.js');
// Obj ID
const ObjectId = require('mongodb').ObjectID;

// Add data to campaignInfluencerModel
const createData = (bodyData, callback) => {
    campaignInfluencerModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}

// get data / find by element - returns only one record
const getData = (bodyData, callback) => {
    campaignInfluencerModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

// get data / find by element - returns only one record
const getCamInfData = (bodyData, callback) => {
    campaignInfluencerModel.find(bodyData, (err, result) => {
        callback(err, result);
    });
}

// find data / find by element - returns only one record
const findData = (bodyData, callback) => {
    campaignInfluencerModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

// find data / find by element - returns only one record
const findDataUnique = (bodyData, callback) => {
    campaignInfluencerModel.find(bodyData).distinct('influencer_id', function(err, result) {
        callback(err, result);
    });
}

// update only one record
const updateOne = (query, bodyData, options, callback) => {
    campaignInfluencerModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

// update only one record
const getDetails = (bodydata, options, callback) => {
    campaignInfluencerModel.findOne(bodydata, options, (err, result) => {
        callback(err, result);
    });
}


const getDetails_pagination = (bodydata, options, page, callback) => {
    campaignInfluencerModel.findOne(bodydata, options, page, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

const updateDetails = (query, bodydata, options, callback) => {
    campaignInfluencerModel.findByIdAndUpdate(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

const deleteDetails = (query, options, callback) => {
    campaignInfluencerModel.remove(query, options, (err, result) => {
        callback(err, result);
    });
}

// find by document id and update and push item in array
const arrayUpdateRemove = (query, data, options, callback) => {
    campaignInfluencerModel.findByIdAndUpdate(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

// find by document id and array key and set item in array
const arrayUpdate = (query, data, options, callback) => {
    campaignInfluencerModel.updateOne(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

const getOnlyData = (data, options, callback) => {
    campaignInfluencerModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}

const getAllData = (bodydata, options, callback) => {
    campaignInfluencerModel.find(bodydata, options, (err, result) => {
        callback(err, result);
    }).lean();
}

// Remove record
const removeRecord = (bodydata, options, callback) => {
    campaignInfluencerModel.deleteMany(bodydata, options, (err, result) => {
        callback(err, result);
    });
}

const manageInfluencerListWithPagiation = (bodyData, callback) => {
   campaignInfluencerModel.aggregate([
        { $match: { "campaign_id": bodyData.campaign_id }},
        {$match:{$or:[{ $and:[{ "type": 'micro'}, {"action": {"$ne": ""}}]},{"type": 'premium'}]}},
        bodyData.type,
        bodyData.status,
        bodyData.social,
        {
            $lookup: {
                from: "influencers",
                localField: "influencer_id",
                foreignField: "_id",
                as: "influencerData"
            }
        },
        {
            $match: {
                $or: [{
                    "ELV_Social": {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                },{
                    "influencerData.first_name": {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                },{
                    "influencerData.email": {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                },{
                    "influencerData.mobile": {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                },{
                    "influencerData.country": {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                },{
                    "influencerData.state": {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                },{
                    "influencerData.city": {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }],
            },
        },
        bodyData.gender,
        {
            $group: {
                _id: "$_id",
                "campaign_id": {"$first": "$campaign_id"},
                "influencer_id": {"$first": "$influencer_id"},
                "Elv_social": {"$first": "$Elv_social"},
                "platform": {"$first": "$platform"},
                "type": {"$first": "$type"},
                "category": {"$first": "$category"},
                "currency_code": {"$first": "$currency_code"},
                "cost": {"$first": "$cost"},
                "package_cost": {"$first": "$package_cost"},
                "premium_brief": {"$first": "$premium_brief"},
                "post_done": {"$first": "$post_done"},
                "post_needed": {"$first": "$post_needed"},
                "action": {"$first": "$action"},
                "created_at": {"$first": "$created_at"},
                "updated_at": {"$first": "$updated_at"},
                "status": {"$first": "$status"},
                "influencerData": {"$max": "$influencerData"}
            },

        }
    ]).exec((error, success) => {;
        callback(error, success);
    });
}

const getAllDataAsync = async (bodydata, options) => {
    return await campaignInfluencerModel.find(bodydata, options);
}

const deleteDetailsAsync = async (query) => {
    return await campaignInfluencerModel.deleteMany(query)
}

module.exports = {
    createData,
    getData,
    updateOne,
    getDetails,
    updateDetails,
    deleteDetails,
    getDetails_pagination,
    findData,
    arrayUpdateRemove,
    getOnlyData,
    arrayUpdate,
    getAllData,
    removeRecord,
    findDataUnique,
    manageInfluencerListWithPagiation,
    getCamInfData,
    getAllDataAsync,
    deleteDetailsAsync
}