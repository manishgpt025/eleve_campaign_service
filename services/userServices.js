const userModel = require('../models/userModel.js');
const ObjectId = require('mongodb').ObjectID;
// Add data to userModel
const createData = (bodyData, callback) => {
    userModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}

// get data / find by element - returns only one record
const getData = (bodyData, callback) => {
    userModel.findOne(bodyData, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

// find data / find by element - returns only one record
const findData = (bodyData, callback) => {
    console.log(bodyData)
    userModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

// update only one record
const updateOne = (query, bodyData, options, callback) => {
    userModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

// update only one record
const getDetails = (bodydata, options, callback) => {
    userModel.findOne(bodydata, options, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

//
const getDetails_pagination = (bodydata, options, page, callback) => {
    userModel.findOne(bodydata, options, page, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

//
const updateDetails = (query, bodydata, options, callback) => {
    userModel.findByIdAndUpdate(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

//
const deleteDetails = (query, bodydata, options, callback) => {
    userModel.findByIdAndRemove(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

// find by document id and update and push item in array
const arrayUpdateRemove = (query, data, callback) => {
    userModel.findByIdAndUpdate(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

// find by document id and array key and set item in array
const arrayUpdate = (query, data, callback) => {
    userModel.updateOne(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

//
const getOnlyData = (data, options, callback) => {
    userModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}

// Get all data with pagination using aggregate
const getPaginateDataWithAggregate = (bodyData, options, sort, callback) => {
    var aggregate = userModel.aggregate([{
            $match: { "registered": '1' }
        },
        {
            $match: { "status": bodyData.status }
        },
        {
            $match: { gender: { $in: bodyData.gender } }
        },
        {
            $match: {
                $or: [{
                    first_name: {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }, {
                    last_name: {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }, {
                    email: {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }, {
                    country: {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }, {
                    state: {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }, {
                    city: {
                        $regex: bodyData.search,
                        $options: 'i'
                    }
                }],
                // $or: [{
                //     gender: {
                //         $in: bodyData.gender
                //     }
                // }],
                // $and: [{
                //     facebook: {
                //         $in: bodyData.social
                //     }
                // },{
                //     twitter: {
                //         $in: bodyData.social
                //     }
                // },{
                //     instagram: {
                //         $in: bodyData.social
                //     }
                // },{
                //     blog: {
                //         $in: bodyData.social
                //     }
                // },{
                //     youtube: {
                //         $in: bodyData.social
                //     }
                // }]
            },
        },

        {
            $match: {
                $and: [{
                        facebook: bodyData.facebook,

                    },
                    {
                        twitter: bodyData.twitter,

                    },
                    {
                        instagram: bodyData.instagram,

                    },
                    {
                        blog: bodyData.blog,

                    },
                    {
                        youtube: bodyData.youtube,

                    },
                    {
                        snapchat: bodyData.snapchat,

                    },
                    {
                        tiktok: bodyData.tiktok,

                    }
                ]
            }
        },
        // {
        //     $unwind: {
        //         path: "$facebook",
        //         preserveNullAndEmptyArrays: true
        //     }
        // },

        {
            $match: {
                $or: [{
                        'facebook.category': {
                            $in: bodyData.category
                        }
                    },
                    {
                        'twitter.category': {
                            $in: bodyData.category
                        }
                    },
                    {
                        'instagram.category': {
                            $in: bodyData.category
                        }
                    },
                    {
                        'blog.category': {
                            $in: bodyData.category
                        }
                    },
                    {
                        'youtube.category': {
                            $in: bodyData.category
                        }
                    },
                    {
                        'tiktok.category': {
                            $in: bodyData.category
                        }
                    },
                    {
                        'snapchat.category': {
                            $in: bodyData.category
                        }
                    },

                ]
            }
        },

        // {
        //     "$project": {
        //         "_id": 1,
        //         "first_name": 1

        //     }
        // },
        // { $sort: sort }

    ])
    userModel.aggregatePaginate(aggregate, options, (error, success, pages, total) => {
        callback(error, success, pages, total);
    })
}

//
const getInfluencers = (bodyData, options, callback) => {

    userModel.aggregate([{
                $match: { "registered": '1' }
            },
            {
                $match: { "status": "1" }
            },
            {
                $match: { "facebook.category": "A" }
            },
            {
                $match: {
                    $or: [{
                            'facebook.Elv_social': {
                                $regex: bodyData.search,
                                $options: 'i'
                            }
                        },
                        {
                            'twitter.Elv_social': {
                                $regex: bodyData.search,
                                $options: 'i'
                            }
                        },
                        {
                            'instagram.Elv_social': {
                                $regex: bodyData.search,
                                $options: 'i'
                            }
                        },
                        {
                            'blog.Elv_social': {
                                $regex: bodyData.search,
                                $options: 'i'
                            }
                        },
                        {
                            'youtube.Elv_social': {
                                $regex: bodyData.search,
                                $options: 'i'
                            }
                        },
                        {
                            'tiktok.Elv_social': {
                                $regex: bodyData.search,
                                $options: 'i'
                            }
                        },
                        {
                            'snapchat.Elv_social': {
                                $regex: bodyData.search,
                                $options: 'i'
                            }
                        },

                    ]
                }
            }
        ])
        // userModel.aggregate(aggregate, options, (error, success) => {
        //     callback(error, success);
        // })
        .exec((err, result) => {
            console.log(err);
            callback(err, result);
        });
}
const getUserDetails = (bodydata, options, callback) => {
    userModel.find(bodydata, options, (err, result) => {
        callback(err, result);
    });
}

const getInfluencers_bak = (bodyData, options, callback) => {
    console.log("Option ==> ", options)
    var platform = options.platform;
    userModel.aggregate([{
                $match: { "registered": '1' }
            },
            {
                $match: { "status": "1" }
            },
            {
                $match: {
                    $or: [{
                            'facebook.Elv_social': {
                                $regex: bodyData.search,
                                $options: 'i'
                            }
                        }

                    ]
                }
            }
        ])
        // userModel.aggregate(aggregate, options, (error, success) => {
        //     callback(error, success);
        // })
        .exec((err, result) => {
            console.log(err);
            callback(err, result);
        });
}

//
const getInfluencerData = (bodyData, options, callback) => {
    userModel.find(bodyData, options, (err, result) => {
        callback(err, result);
    });
}

// Premium influencer search
const premiumInfluncerSearch = (bodyData, options, callback) => {
   userModel.aggregate([{
            $match: { "registered": '1' }
        },
        {
            $match: { "status": "1" }
        },
        {
            $match: bodyData
        }
    ]).exec((err, result) => {
        callback(err, result);
    });
}

//manage kol micro search

const microInfluencerSearch = (bodydata, options, skip, limit, callback) => {
        userModel.find(bodydata, options, (err, result) => {
            callback(err, result);
        }).skip(skip).limit(limit);
    }

//manage kol micro search count

const microInfluencerSearchCount = (bodydata, options,callback) => {
        userModel.count(bodydata, options, (err, result) => {
            callback(err, result);
        });
    }

    // Export
module.exports = {
    "createData": createData,
    "getData": getData,
    "updateOne": updateOne,
    "getDetails": getDetails,
    "updateDetails": updateDetails,
    "deleteDetails": deleteDetails,
    "getDetails_pagination": getDetails_pagination,
    "findData": findData,
    "arrayUpdateRemove": arrayUpdateRemove,
    "getOnlyData": getOnlyData,
    "arrayUpdate": arrayUpdate,
    "getPaginateDataWithAggregate": getPaginateDataWithAggregate,
    "getInfluencers": getInfluencers,
    "getUserDetails": getUserDetails,
    "getInfluencerData": getInfluencerData,
    "getInfluencers_bak": getInfluencers_bak,
    "premiumInfluncerSearch": premiumInfluncerSearch,
    "microInfluencerSearch": microInfluencerSearch,
    microInfluencerSearchCount
}