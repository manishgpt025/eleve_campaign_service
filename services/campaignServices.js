const campaignModel = require('../models/campaign.js');

// Add data to campaignModel
const createData = (bodyData, callback) => {
        campaignModel.create(bodyData, (err, result) => {
            callback(err, result);
        });
    }
    // get data / find by element - returns only one record
const getData = (bodyData, callback) => {
    campaignModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

// find data / find by element - returns only one record
const findData = (bodyData, callback) => {
    campaignModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

// find data / find by element - returns only one record
const findAllData = (bodyData, callback) => {
    campaignModel.find(bodyData, (err, result) => {
        callback(err, result);
    });
}

// update only one record
const updateOne = (query, bodyData, options, callback) => {
    campaignModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

// update only one record
const getDetails = (bodydata, options, callback) => {
    campaignModel.findOne(bodydata, options, (err, result) => {
        callback(err, result);
    });
}


const getDetails_pagination = (bodydata, options, page, callback) => {
    campaignModel.findOne(bodydata, options, page, (err, result) => {
        callback(err, result);
    });
}

const updateDetails = (query, bodydata, options, callback) => {
    campaignModel.findByIdAndUpdate(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

const deleteDetails = (query, bodydata, options, callback) => {
    campaignModel.findByIdAndRemove(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

// find by document id and update and push item in array
const arrayUpdateRemove = (query, data, options, callback) => {
    campaignModel.findByIdAndUpdate(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

// find by document id and array key and set item in array
const arrayUpdate = (query, data, options, callback) => {
    campaignModel.updateOne(query, data, options, (err, result) => {
        callback(err, result);
    });
}

const getOnlyData = (data, options, callback) => {
    campaignModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}

const getAllData = (bodydata, options, callback) => {
    campaignModel.find(bodydata, options, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

const deletedescriptionDetail = async(bodydata) => {
    await campaignModel.findOneAndUpdate({ "_id": bodydata.campaign_id, "male_description.platform": bodydata.platform }, { $pull: { "male_description": { "platform": bodydata.platform } } }, { safe: true, new: true });
    await campaignModel.findOneAndUpdate({ "_id": bodydata.campaign_id, "female_description.platform": bodydata.platform }, { $pull: { "female_description": { "platform": bodydata.platform } } }, { safe: true, new: true });
    await campaignModel.findOneAndUpdate({ "_id": bodydata.campaign_id, "other_description.platform": bodydata.platform }, { $pull: { "other_description": { "platform": bodydata.platform } } }, { safe: true, new: true });
    await campaignModel.findOneAndUpdate({ "_id": bodydata.campaign_id, "male_sample.platform": bodydata.platform }, { $pull: { "male_sample": { "platform": bodydata.platform } } }, { safe: true, new: true });
    await campaignModel.findOneAndUpdate({ "_id": bodydata.campaign_id, "female_sample.platform": bodydata.platform }, { $pull: { "female_sample": { "platform": bodydata.platform } } }, { safe: true, new: true });
    await campaignModel.findOneAndUpdate({ "_id": bodydata.campaign_id, "other_sample.platform": bodydata.platform }, { $pull: { "other_sample": { "platform": bodydata.platform } } }, { safe: true, new: true });
    await campaignModel.findOneAndUpdate({ "_id": bodydata.campaign_id, "premium_common_brief.platform": bodydata.platform }, { $pull: { "premium_common_brief": { "platform": bodydata.platform } } }, { safe: true, new: true });
    return true
}

// Get all admin with pagination using aggregate
const getPaginateDataWithAggregate = (bodyData, options, sort, callback) => {
    var aggregate = campaignModel.aggregate([
        {
            $lookup: {
                from: "admin_users",
                localField: "created_id",
                foreignField: "_id",
                as: "admin_details"
            }
        },
        {
            $match: {
                $or: [{
                        title: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        brand_name: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        organization_name: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        hashtag: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        created_by: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        'admin_details.name': {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },

                ]
            },
        },
        bodyData.status,
        bodyData.platform,
        {
            $project : {
                "advertiser_brand_name": 1,
                "advertiser_name": 1,
                "organization_name": 1,
                "status": 1,
                "title": 1,
                "type": 1,
                "campaign_id": 1,
                "platforms": 1,
                "end_date": 1,
                "start_date": 1,
                "hashtag": 1
            }
        },
        sort
    ])
    campaignModel.aggregatePaginate(aggregate, options, (error, success, pages, total) => {
        callback(error, success, pages, total);
    })


}

const getAggregate = (bodyData, callback) => {
    campaignModel.aggregate([{
            $match: bodyData,
        },
        {
            $lookup: {
                from: "campaign_influencers",
                localField: "_id",
                foreignField: "campaign_id",
                as: "influencer_details"
            }
        },
        {
            $project: {
                "title": 1,
                "hashtahg_share": 1,
                "url_share": 1,
                "secondary_hashtag": 1,
                "track_secondary_hashtag": 1,
                "keywords": 1,
                "influencer_details": 1,
                "influencers_count": { $size: "$influencer_details" },

            }
        }
    ]).exec((err, result) => {
        console.log(err);
        callback(err, result);
    });
}

const findCampaignData = (bodyData, options, callback) => {
    campaignModel.findOne(bodyData, options, (err, result) => {
        callback(err, result);
    });
}

// update only one record async
const updateOneAsync = async (query, bodyData, options) => {
    return await campaignModel.findOneAndUpdate(query, bodyData, options);
}

module.exports = {
        createData,
        getData,
        updateOne,
        getDetails,
        updateDetails,
        deleteDetails,
        getDetails_pagination,
        findData,
        arrayUpdateRemove,
        getOnlyData,
        arrayUpdate,
        getAllData,
        getPaginateDataWithAggregate,
        getAggregate,
        deletedescriptionDetail,
        findCampaignData,
        findAllData,
        updateOneAsync
}