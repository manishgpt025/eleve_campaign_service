const adminModel = require('../models/adminModel.js');

const createData = (bodyData, callback) => {
    adminModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}

const getData = (bodyData, callback) => {
    adminModel.find(bodyData, (err, result) => {
        callback(err, result);
    });
}

const findData = (bodyData, callback) => {
    adminModel.findOne(bodyData, (err, result) => {
        callback(err, result);
    });
}

const updateOne = (query, bodyData, options, callback) => {
    adminModel.findOneAndUpdate(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}

const getDetails = (bodydata, options, callback) => {
    adminModel.findOne(bodydata, options, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

const getDetails_pagination = (bodydata, options, page, callback) => {
    adminModel.findOne(bodydata, options, page, { password: 0 }, (err, result) => {
        callback(err, result);
    });
}

const updateDetails = (query, bodydata, options, callback) => {
    adminModel.findByIdAndUpdate(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

const deleteDetails = (query, bodydata, options, callback) => {
    adminModel.findByIdAndRemove(query, bodydata, options, (err, result) => {
        callback(err, result);
    });
}

const getOnlyData = (data, options, callback) => {
    adminModel.findOne(data, options, (err, result) => {
        callback(err, result);
    });
}

const arrayUpdate = (query, data, options, callback) => {
    adminModel.updateOne(query, data, { safe: true, upsert: true }, (err, result) => {
        callback(err, result);
    });
}

// Get record by pagination
const getPaginateData = (data, options, sort, callback) => {
    adminModel.paginate(data, options, sort, (err, result) => {
        callback(err, result);
    });
}

const findstatusData = (bodyData, options, callback) => {
    adminModel.findOne(bodyData, options, (err, result) => {
        callback(err, result);
    });
}

// Get all admin with pagination using aggregate
const getPaginateDataWithAggregate = (bodyData, options, sort, callback) => {
    var aggregate = adminModel.aggregate([{
            $match: {
                $or: [{
                        name: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }, {
                        email: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        team: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: bodyData.search,
                            $options: 'i'
                        }
                    }
                ]
            },
        },
        {
            $match: { status: { $in: bodyData.status } },
        },
        {
            $match: { role: { $in: bodyData.role } }
        },
        {
            $match: { location: { $in: bodyData.location } }
        },
        { $sort: sort }

    ])
    adminModel.aggregatePaginate(aggregate, options, (error, success, pages, total) => {
        callback(error, success, pages, total);
    })


}

module.exports = {
    "createData": createData,
    "getData": getData,
    "updateOne": updateOne,
    "getDetails": getDetails,
    "updateDetails": updateDetails,
    "deleteDetails": deleteDetails,
    "getDetails_pagination": getDetails_pagination,
    "findData": findData,
    "getOnlyData": getOnlyData,
    "arrayUpdate": arrayUpdate,
    "getPaginateData": getPaginateData,
    "findstatusData": findstatusData,
    "getPaginateDataWithAggregate": getPaginateDataWithAggregate
}