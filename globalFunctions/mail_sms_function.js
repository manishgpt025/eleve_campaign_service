const campaignModel = require('../models/campaign.js');
const influencerModel = require('../models/userModel.js');
const campaignInfluencerModel = require('../models/campaign_influencer.js');
const campaignInfluencerService = require('../services/campaignInfluencerServices.js'); // campaign influencer services
//use user model
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('CG0lEmHWZK4req6olYnGOQ');

var AWS = require('aws-sdk');
AWS.config.loadFromPath('./s3_config.json');


// find all participate influencer in live campaign
const live_campaign_notification = async(campaignId ,type, emailStatus, smsstatus,  emailSender, emailBody, emailSubject, smsBody) => {
    let condition =  {'campaign_id': campaignId,'type': type}
    let campaigninfluencersIds = await camapignInfluencers(condition);
    if(campaigninfluencersIds.length){
        let influencersData = await influencersDetail(campaigninfluencersIds);
        if(influencersData.length){
            if(emailStatus == '1') {
                sendEmail(campaignId,influencersData , emailSubject, emailBody, emailSender);
            }
            if(smsstatus == '1') {
                sendSms(campaignId,influencersData , smsBody);
            }
        }

    }
}

// find accepted influencer in live campaign (step one and three updates)
const update_basic_detail_campaign_notification = async(campaignId ,type, emailStatus, smsstatus,  emailSender, emailBody, emailSubject, smsBody) => {
    let condition =  {'campaign_id': campaignId, 'action':'accepted','type': type}
    let campaigninfluencersIds = await camapignInfluencers(condition);
    if(campaigninfluencersIds.length){
        let influencersData = await influencersDetail(campaigninfluencersIds);
        if(influencersData.length){
            if(emailStatus == '1') {
                sendEmail(campaignId,influencersData , emailSubject, emailBody, emailSender);
            }
            if(smsstatus == '1') {
                sendSms(campaignId,influencersData , smsBody);
            }
        }

    }
}

// find accepted influencer in live campaign
const update_micro_campaign_notification = async(campaignId ,type,platform, emailStatus, smsstatus,  emailSender, emailBody, emailSubject, smsBody) => {
    let condition =  {'campaign_id': campaignId, 'action':'accepted', 'platform':platform,'type': type}
    let campaigninfluencersIds = await camapignInfluencers(condition);
    if(campaigninfluencersIds.length){
        let influencersData = await influencersDetail(campaigninfluencersIds);
        if(influencersData.length){
            if(emailStatus == '1') {
                sendEmail(campaignId,influencersData , emailSubject, emailBody, emailSender);
            }
            if(smsstatus == '1') {
                sendSms(campaignId,influencersData , smsBody);
            }
        }

    }
}

// find new influencer in live campaign
const update_new_influencer_notification = async (campaigninfluencersIds,campaignId, emailStatus, smsstatus, emailSender, emailBody, emailSubject, smsBody) => {
    if(campaigninfluencersIds.length){
        let influencersData = await influencersDetail(campaigninfluencersIds);
        if(influencersData.length){
            if(emailStatus == '1') {
                sendEmail(campaignId,influencersData , emailSubject, emailBody, emailSender);
            }
            if(smsstatus == '1') {
                sendSms(campaignId, influencersData, smsBody);
            }
        }
    }
}

// find new influencer in live campaign
const update_premium_campaign_notification = async (campaigninfluencersIds,campaignId, emailStatus, smsstatus, emailSender, emailBody, emailSubject, smsBody) => {
    if(campaigninfluencersIds.length){
        let influencersData = await influencersDetail(campaigninfluencersIds);
        if(influencersData.length){
            if(emailStatus == '1') {
                sendEmail(campaignId,influencersData , emailSubject, emailBody, emailSender);
            }
            if(smsstatus == '1') {
                sendSms(campaignId,influencersData , smsBody);
            }
        }
    }
}

// Get campaign influencers list
camapignInfluencers = async (condition) => {
    let influencers = [];
    result = await campaignInfluencerService.getAllDataAsync(condition,{influencer_id:1});
        if (result.length > 0) {
            result.forEach(function(results) {
                influencers.push(
                    results.influencer_id
                );
            });
            return influencers;
        }
        return influencers;
}

//Get influencer email and mobile number
influencersDetail = async (influencerIds) => {
    let result = '';
    result = await influencerModel.find({'_id' : {'$in' : influencerIds}},{'first_name':1, 'email' : 1,'mobile' : 1,'isd_code' : 1});
        if (result.length > 0) {
            return result;
        }
        return result;
}

// send email to influencers
function sendEmail(campaignId, result , emailSubject, emailBody, emailSender) {
    let link = sortnerUrl(campaignId);
    result.forEach(function(inf_res, index) {
        let emailReplaceData = emailBody;
        emailReplaceData = emailReplaceData.replace('&lt;influencer_name&gt;<influencer_name>',inf_res.first_name);
        emailReplaceData = emailReplaceData.replace('&lt;link&gt; <link>',link);
		var message = {
			"html": emailReplaceData,
			"subject": emailSubject,
			"from_email": emailSender,
			"from_name": "Eleve",
			"to": [{
				"email": inf_res.email,
				"type": "to"
			}]
		}
		let sendmail = sendEmailFormat(message);
	});
}

// Send Sms to influencers
function sendSms(campaignId, result , smsBody) {
    let link = sortnerUrl(campaignId);
    smsBody = smsBody.replace('<link>',link);
	result.forEach(function(inf_res) {
	   let data = {
			mobile: inf_res.isd_code + inf_res.mobile,
			body: smsBody
		}
		let resp = sendSMSEmail(data);
	});
}

//email format
function sendEmailFormat(obj){
    console.log('send email', obj)
    var message = obj;
    var async = false;
    if(Object.keys(message).length){
        ({to:[{email:email_format,type}]} = message);
        let extention = email_format.split('@')[1];
        // TODO:: we need to remove these check when we are working for live
        if(
            !extention.includes('eleve')
               &&
            !extention.includes('engagelyee')
        ){
            message = {...message, to:[{email:'mohan@eleve.co.in',type}]}
        }
    }
    mandrill_client.messages.send({
        "message": message,
        "async": async
    }, function(result) {
        return result;
    }, function(e) {
        return e.message;
    });
}

// sms format
function sendSMSEmail(obj) {
    console.log('send sms', obj)
	let params = {
       // PhoneNumber: obj.mobile,
        PhoneNumber: '+919971503285',
        Message: obj.body,
        MessageStructure: 'string',
    };
    let aws_client = new AWS.SNS({
        accessKeyId:process.env.ACCESSKEY,
        secretAccessKey:process.env.SECRETACCESSKEY,
        region:process.env.REGION
    });

    aws_client.publish(params, (err, data) => {
        if (err) {
            console.log('in sns error');
            throw err;
        }
        console.log(data)
    });
}

//Get sortner url
function sortnerUrl(id){
   let bind_url =  'dev.eleveglobal.com/campaign/new/'+ id;
    return bind_url;
}


module.exports = {
    live_campaign_notification,
    update_basic_detail_campaign_notification,
    update_micro_campaign_notification,
    update_new_influencer_notification,
    update_premium_campaign_notification
}