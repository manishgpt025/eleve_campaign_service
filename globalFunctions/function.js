const request = require('request-promise');
//use user model
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('CG0lEmHWZK4req6olYnGOQ');
var randomstring = require("randomstring");

var AWS = require('aws-sdk');
AWS.config.loadFromPath('./s3_config.json');
var s3Bucket = new AWS.S3({
    params: {
        Bucket: 'eleve-global'
    }
});


module.exports = {
    /**
     * [Check post key validation]
     */
    checkRequest: (array, obj) => {
        for (i of array) {
            if (obj[i] == undefined || obj[i] == "")
                return i;
        }
        return true;
    },
    /**
     * [ check email from DB]
     */
    checkEmail: (obj) => {
        user.findOne({
            email: obj
        }, (error, result) => {
            if (error) {
                return 0;
            } else {
                return result;
            }
        })
    },
    /**
     * [ check mobile from DB]
     */
    checkMobile: (obj) => {
        user.findOne({
            mobile: obj
        }, (error, result) => {
            if (error) {
                return 0;
            } else {
                return result;
            }
        })
    },
    /**
     * [ sending email function]
     */
    sendEmail: (obj) => {
        //console.log("message ",obj);
        var message = obj;
        var async = false;
        var ip_pool = "";
        var send_at = "";
        mandrill_client.messages.send({
            "message": message,
            "async": async,
            "ip_pool": ip_pool,
            "send_at": send_at
        }, function(result) {
            return result;
        }, function(e) {
            // Mandrill returns the error as an object with name and message keys
            return e.message;
            // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
        });
    },
    /**
     * [ sending sms function]
     */
    sendSMS: (obj) => {
        let sns = new AWS.SNS({
            accessKeyId: 'AKIAJHJ2HVZPRSQ2ONWA',
            secretAccessKey: '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',
            region: 'ap-southeast-1'
        });

        let params = {
            PhoneNumber: obj.mobile,
            Message: 'Your OTP is ' + obj.otp,
            MessageStructure: 'string',
        };

        sns.publish(params, (err, data) => {
            if (err) {
                console.log('in sns error');
                throw err;
            }
            console.log(data)
        });
    },
    /**
     * [ sending resend otp function]
     */
    sendNewOTP: async(obj) => {
        let key = 'Hj1CnqXrlq4ZIGSDdtqneepNvKcSgI';
        const options = {
            method: 'GET',
            uri: 'https://push.sanketik.net//api/push?accesskey=' + key + '&to=' + obj.mobile + '&text=' + 'Hi' + '&from=ELEVEM'
        };
        await request(options).then((response) => {}).catch((err) => {
            console.log(err);
        })
    },
    /**
     * [ upload multipart file function]
     */
    uploadFile: async(userId, file, callback) => {
        let number = Math.random() * (999999 - 10000) + 10000
        let resData = {
            Key: userId + number,
            Body: file,
            ContentType: 'image/jpeg',
            ACL: 'public-read'
        };
        await s3Bucket.upload(resData, (error, result) => {
            if (error) {
                return 0;
            } else {
                callback(null, result.Location);
            }
        });
    },
    /* Send campaign content */
    /**
     * [ sending sms function]
     */
    sendCampaignSMS: (obj) => {
        let sns = new AWS.SNS({
            accessKeyId: 'AKIAJHJ2HVZPRSQ2ONWA',
            secretAccessKey: '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',
            region: 'ap-southeast-1'
        });

        let params = {
            PhoneNumber: obj.mobile,
            Message: obj.body,
            MessageStructure: 'string',
        };

        sns.publish(params, (err, data) => {
            if (err) {
                console.log('in sns error');
                throw err;
            }
            console.log(data)
        });
    },
    //generate Random String
    generateRandomString: () => {
        let newId = randomstring.generate({
            length: 12,
            charset: 'hex'
        });
        return newId = newId + new Date().getTime().toString();
    },
    //Check platform
    checkPlatform: (platformData) =>{
        platform_array = ["twitter", "facebook", "youtube", "instagram"];
        return platform_array.findIndex(platform => platform == platformData) === -1;
    }
}